@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Data Pelanggan</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item">Data Master</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th>No</th>
                  <th>Kode Pelanggan</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Telp</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="text-dark"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/cleave.min.js') }}"></script>
<script src="{{ asset('js/cleave-phone.id.js') }}"></script>
@include('pelanggan/modal')
<script>
  $(document).ready(function() {
    $('#Pelanggan_sidebar').parent().addClass('active');
    $('#Pelanggan_sidebar').parent().parent().parent().addClass('active');

    let cleavePN = new Cleave('.phone-number', {
      phone: true,
      phoneRegionCode: 'id'
    });

    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('pelanggan.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum',
            searchable: false
          },
          {
            data: 'kode_pelanggan',
            name: 'kode_pelanggan'
          },
          {
            data: 'nama',
            name: 'nama'
          },
          {
            data: 'alamat',
            name: 'alamat'
          },
          {
            data: 'no_telp',
            name: 'no_telp'
          },
          {
            data: 'email',
            name: 'email'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah Pelanggan");
        $('#tambah-edit-modal').modal('show');
      });

      if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
          messages: {
            nama: "Nama Pemasok tidak boleh kosong",
            alamat: {
              required: "Alamat tidak boleh kosong",
              minlength: "Alamat harus berisi Jl. No, Kota",
            },
            email: {
              required: "Email tidak boleh kosong",
              email: "Alamat email salah",
            },
            no_telp: {
              required: "Telp tidak boleh kosong",
              minlength: "Telp minimal 8 digit",
            },
          },
          errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").html(error);
          },

          submitHandler: function(form) {
            $('#tombol-simpan').html('Mengirim..');

            $.ajax({
              data: $('#form-tambah-edit').serialize(),
              url: "{{ route('pelanggan.store') }}",
              type: "POST",
              dataType: 'json',
              success: function(response) {
                $('#tombol-simpan').html('Simpan');
                if (response.icon == 'error') {
                  $('#tambah-edit-modal').modal('show');
                  iziToast.error({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                } else {
                  $('#form-tambah-edit').trigger("reset");
                  $('#tambah-edit-modal').modal('hide');
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                }
              },
              error: function(response) {
                console.log('Error:', response);
                $('#tombol-simpan').html('Simpan');
              }
            });
          }
        });
      }

      $(document).on('click', '.editPelanggan', function() {
        $('#myModalLabel').text("Edit Pelanggan");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let kode_pelanggan = $(this).attr("data-kode_pelanggan");
        let nama = $(this).attr("data-nama");
        let alamat = $(this).attr("data-alamat");
        let no_telp = $(this).attr("data-no_telp");
        let email = $(this).attr("data-email");

        $('.modal-body #Id').val(id);
        $('.modal-body #Kode_pelanggan').val(kode_pelanggan);
        $('.modal-body #nama').val(nama);
        $('.modal-body #Alamat').val(alamat);
        $('.modal-body #No_telp').val(no_telp);
        $('.modal-body #Email').val(email);

      });

      $(document).on('click', '.deletePelanggan', function() {
        let id = $(this).attr("data-id");
        let nama = $(this).attr("data-nama");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data "' + nama + '" akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('pelanggan.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                  $('#tombol-simpan').html('Simpan');
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });

      });

    });
  });
</script>

@endsection