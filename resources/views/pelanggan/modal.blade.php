<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <input type="hidden" name="id" id="Id" value="">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="Kode_pelanggan">Kode Pelanggan <span class="text-danger">*</span></label>
              <input id="Kode_pelanggan" type="text" class="form-control" name="kode_pelanggan" value="{{ $kode }}" readonly>
            </div>
            <div class="form-group col-md-8">
              <label for="nama">Nama <span class="text-danger">*</span></label>
              <input id="nama" type="text" class="form-control" name="nama" tabindex="1" maxlength="20" required autofocus>
              <div class="invalid-feedback" id="nama_error"></div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="Email">Email <span class="text-danger">*</span></label>
              <input id="Email" type="email" class="form-control" name="email" tabindex="2" required>
              <div class="invalid-feedback" id="email_error"></div>
            </div>
            <div class="form-group col-md-6">
              <label>No.Telephone <span class="text-danger">*</span></label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <i class="fas fa-phone"></i>
                  </div>
                </div>
                <input type="text" class="form-control phone-number" id="No_telp" name="no_telp" tabindex="3" required>
                <div class="invalid-feedback" id="no_telp_error" style="margin-left: 55px;"></div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="Alamat">Alamat <span class="text-danger">*</span></label>
            <textarea id="Alamat" type="text" class="form-control" name="alamat" tabindex="4" minlength="10" maxlength="30" required></textarea>
            <div class="invalid-feedback" id="alamat_error"></div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="5">Simpan</button>
        <button type="submit" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>