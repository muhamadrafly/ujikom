<div class="main-sidebar sidebar-style-2" style="overflow: hidden; outline: none; cursor: grab; touch-action: none;">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <img src="{{ asset('img/logo.jpg') }}" alt="logo" width="40" class="shadow-light rounded-circle mr-2">
      <a href="javascript:void(0)">ElMart</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="javascript:void(0)">EM</a>
    </div>

    @php
    $checkAccess = App\Models\Acces::where('user_id', Auth::id())->first();
    @endphp

    <ul class="sidebar-menu">
      <li class="menu-header">Menu</li>
      @if($checkAccess->kelola_laporan == 1)
      <li><a class="nav-link" id="Dashboard_sidebar" href="{{ url('menu/home') }}"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
      @endif
      <li class="dropdown">
        <a href="javascript:void(0)" class="nav-link has-dropdown"><i class="fas fa-pallet"></i><span>Master Data</span></a>
        <ul class="dropdown-menu">
          @if($checkAccess->kelola_barang == 1)
          <li><a class="nav-link" id="Produk_sidebar" href="{{ url('menu/master_data/produk') }}">Produk</a></li>
          <li><a class="nav-link" id="Barang_sidebar" href="{{ url('menu/master_data/barang') }}">Barang</a></li>
          @endif
          @if($checkAccess->kelola_pasok == 1)
          <li><a class="nav-link" id="Pemasok_sidebar" href="{{ url('menu/master_data/pemasok') }}">Pemasok</a></li>
          @endif
          @if($checkAccess->kelola_penjualan == 1)
          <li><a class="nav-link" id="Pelanggan_sidebar" href="{{ url('menu/master_data/pelanggan') }}">Pelanggan</a></li>
          @endif
        </ul>
      </li>


      <li class="dropdown">
        <a href="javascript:void(0)" class="nav-link has-dropdown"><i class="fas fa-cash-register"></i><span>Transaksi</span></a>
        <ul class="dropdown-menu">
          @if($checkAccess->kelola_pasok == 1)
          <li><a class="nav-link" id="Pembelian_sidebar" href="{{ url('menu/transaksi/pembelian') }}">Pembelian</a></li>
          @endif
          @if($checkAccess->kelola_penjualan == 1)
          <li><a class="nav-link" id="Penjualan_sidebar" href="{{ url('menu/transaksi/penjualan') }}">Penjualan</a></li>
          @endif
        </ul>
      </li>

      <!-- @if($checkAccess->kelola_pasok == 1)
      <li><a class="nav-link" id="Pembelian_sidebar" href="{{ url('menu/transaksi/pembelian') }}"><i class="fas fa-shipping-fast"></i><span>Pembelian</span></a></li>
      @endif
      @if($checkAccess->kelola_penjualan == 1)
      <li><a class="nav-link" id="Penjualan_sidebar" href="{{ url('menu/transaksi/penjualan') }}"><i class="fas fa-cash-register"></i><span>Penjualan</span></a></li>
      @endif -->
      @if($checkAccess->kelola_barang == 1)
      <li><a class="nav-link" id="Penarikan_barang_sidebar" href="{{ url('menu/penarikan_barang') }}"><i class="fas fa-truck-loading"></i><span>Penarikan Barang</span></a></li>
      <li><a class="nav-link" id="Pengajuan_barang_sidebar" href="{{ url('menu/pengajuan_barang') }}"><i class="fas fa-handshake"></i><span>Pengajuan Barang</span></a></li>
      @endif
      @if($checkAccess->kelola_laporan == 1)
      <li class="dropdown">
        <a href="javascript:void(0)" class="nav-link has-dropdown"><i class="fas fa-swatchbook"></i><span>Laporan</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" id="Laporan_pembelian_sidebar" href="{{ url('menu/laporan/laporan_pembelian') }}">Laporan Pembelian</a></li>
          <li><a class="nav-link" id="Laporan_penjualan_sidebar" href="{{ url('menu/laporan/Laporan_penjualan_sidebar') }}">Laporan Penjualan</a></li>
        </ul>
      </li>
      @endif

      @if(Auth::user()->is_admin == 1)
      <li class="menu-header">Admin Menu</li>
      <li class="dropdown">
        <a href="javascript:void(0)" class="nav-link has-dropdown"><i class="far fa-user"></i><span>User Manage</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" id="Account_sidebar" href="{{ url('admin/user_manage/account') }}">Account</a></li>
          <li><a class="nav-link" id="Access_sidebar" href="{{ url('admin/user_manage/access') }}">Access</a></li>
        </ul>
      </li>
      @endif
    </ul>
  </aside>
</div>