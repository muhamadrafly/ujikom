<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <link rel="icon" href="{{ asset('img/logo.jpg') }}">

  <title>Em &mdash; {{ $title }}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

  <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/select2.css') }}" rel="stylesheet">

  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/components.css') }}" rel="stylesheet">
  @yield('css')

</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      @include('templates/topbar')
      @include('templates/sidebar')

      <!-- Main Content -->
      <div class="main-content" style="min-height: 1065px;">
        <section class="section">
          @yield('content')
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright © {{ date('Y') }}
          <div class="bullet"></div> ElMart
          </a>
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>


  <script src="{{ asset('js/iziToast.min.js') }}"></script>
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/dataTables.min.js') }}"></script>

  <script src="{{ asset('js/popper.js') }}"></script>
  <script src="{{ asset('js/tooltip.js') }}"></script>
  <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/stisla.js') }}"></script>

  <script src="{{ asset('js/jquery.form-validator.min.js')}}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('js/select2.js') }}"></script>

  <script src="{{ asset('js/scripts.js') }}"></script>
  @yield('scripts')

</body>

</html>