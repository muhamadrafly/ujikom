<nav class="navbar navbar-expand-lg main-navbar">
  <form class="form-inline mr-auto">
    <ul class="navbar-nav">
      <li><a href="javascript:void(0)" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
  </form>
  <ul class="navbar-nav navbar-right">

    <li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        <img alt="image" src="{{ asset('img/default_profile.jpg') }}" class="rounded-circle mr-1">
        @php
        $userNama = Auth::user()->nama;
        if(strlen($userNama) > 12){
        $nama = substr($userNama, 0, 12) . ' . . .';
        }else{
        $nama = $userNama;
        }
        @endphp
        <div class="d-sm-none d-lg-inline-block">Hi, {{ $nama }}</div>
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-title">User Menu</div>
        <a href="{{ url('menu/profile') }}" class="dropdown-item has-icon">
          <i class="far fa-user"></i> Profile
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ url('menu/logout') }}" class="dropdown-item has-icon text-danger">
          <i class="fas fa-sign-out-alt"></i> Logout
        </a>
      </div>
    </li>
  </ul>
</nav>