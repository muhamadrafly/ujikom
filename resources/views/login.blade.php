<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>ElMart &mdash; Login</title>

  <link rel="icon" href="{{ asset('img/logo.jpg') }}">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">

  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/components.css') }}" rel="stylesheet">

  <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              <img src="{{ asset('img/logo.jpg') }}" alt="logo" width="60" class="shadow-light rounded-circle mr-3">
              <span style="font-size: 15px;">ElMart</span>
              <h5 class="mt-3">Point Of Sales</h5>
            </div>
            <div class="card card-primary" style="margin-top: -20px;">
              <div class="card-body">
                <form class="needs-validation" novalidate="" action="{{ route('auth.store') }}" method="post">
                  @csrf
                  <div class="form-group">
                    <label for="username">Username</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-user"></i>
                        </div>
                      </div>
                      <input id="username" type="username" class="form-control" name="username" tabindex="1" required autofocus>
                      <div class="invalid-feedback" style="margin-left: 45px;">Username tidak boleh kosong!</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <i class="fas fa-lock"></i>
                        </div>
                      </div>
                      <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                      <div class="invalid-feedback" style="margin-left: 45px;">Password tidak boleh kosong!</div>
                    </div>

                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="3">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>

            <div class="simple-footer">
              Copyright © ElMart {{ date('Y') }}
            </div>

          </div>
        </div>
      </div>
    </section>
  </div>
  <script src="{{ asset('js/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/stisla.js') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>

  <script>
    @if(Session('fail'))
    Swal.fire({
      title: 'Gagal',
      text: '{{ Session("fail") }}',
      icon: 'error',
    })
    @endif
  </script>
</body>

</html>