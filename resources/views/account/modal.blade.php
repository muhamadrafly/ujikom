<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <div class="form-row">
            <input type="hidden" name="id" id="Id" value="">
            <div class="form-group col-md-6">
              <label for="nama">Nama <span class="text-danger">*</span></label>
              <input id="Nama" type="text" class="form-control" name="nama" tabindex="1" required autofocus>
              <div class="invalid-feedback" id="nama_error"></div>
            </div>
            <div class="form-group col-md-6">
              <label for="username">Username <span class="text-danger">*</span></label>
              <input id="Username" type="text" class="form-control" name="username" minlength="5" maxlength="12" tabindex="2" required>
              <div class="invalid-feedback" id="username_error"></div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="email">Email <span class="text-danger">*</span></label>
              <input id="Email" type="email" class="form-control" name="email" tabindex="3" required>
              <div class="invalid-feedback" id="email_error"></div>
            </div>
            <div class="form-group col-md-6">
              <label for="role">Posisi <span class="text-danger">*</span></label>
              <select class="form-control custom-select" name="is_admin" id="Is_admin" tabindex="4" required>
                <option value="">Pilih..</option>
                <option value="1">Administrator</option>
                <option value="0">Cashier</option>
              </select>
              <div class="invalid-feedback" id="is_admin_error"></div>
            </div>
          </div>

          <div class="form-group">
            <label for="password">Password <span class="text-danger">*</span></label>
            <input id="Password" type="password" class="form-control" name="password" minlength="8" tabindex="5" required>
            <div class="invalid-feedback" id="password_error"></div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="6">Simpan</button>
        <button type="submit" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>