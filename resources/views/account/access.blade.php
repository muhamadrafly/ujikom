@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Admin</div>
    <div class="breadcrumb-item">User Manage</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">Access</a></div>
  </div>
</div>
<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th>User Detail</th>
                  <th>Kelola Barang</th>
                  <th>Kelola Pasok</th>
                  <th>Kelola Transaksi</th>
                  <th>Kelola Laporan</th>
                </tr>
              </thead>
              <tbody class="text-dark">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    $('#Access_sidebar').parent().addClass('active');
    $('#Access_sidebar').parent().parent().parent().addClass('active');
    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('access.index') }}",
        columns: [{
            data: 'user_id',
            name: 'user_id'
          },
          {
            data: 'kelola_barang',
            name: 'kelola_barang',
            orderable: false,
            searchable: false
          },
          {
            data: 'kelola_pasok',
            name: 'kelola_pasok',
            orderable: false,
            searchable: false
          },
          {
            data: 'kelola_penjualan',
            name: 'kelola_penjualan',
            orderable: false,
            searchable: false
          },
          {
            data: 'kelola_laporan',
            name: 'kelola_laporan',
            orderable: false,
            searchable: false
          },
        ],
      });

      $(document).on('click', 'input[type="checkbox"]', function(e) {
        e.preventDefault();
        let formData = {
          id: $(this).attr('data-user_id'),
          access: $(this).attr('data-access'),
        };
        if ($(this).attr('data-is_admin') == 1) {
          iziToast.warning({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Yakin?',
            message: 'System menyarankan anda, untuk tidak mengubah access Administrator',
            position: 'center',
            buttons: [
              ['<button><b>Ya, </b>Ubah</button>', function(instance, toast) {
                iziToast.success({
                  title: 'Change Success',
                  message: 'Access Berhasil Diubah..',
                  position: 'topRight',
                });
                $.ajax({
                  data: formData,
                  url: "{{ route('access.store') }}",
                  type: "POST",
                  success: function(response) {
                    table.draw()
                  },
                  error: function(response) {
                    console.log("Error: ", response)
                  }
                });

                instance.hide({
                  transitionOut: 'fadeOut'
                }, toast, 'button');

              }, true],
              ['<button>Batal</button>', function(instance, toast) {

                instance.hide({
                  transitionOut: 'fadeOut'
                }, toast, 'button');

              }],
            ],
          });

        } else {
          iziToast.success({
            title: 'Change Success',
            message: 'Access Berhasil Diubah..',
            position: 'topRight',
          });
          $.ajax({
            data: formData,
            url: "{{ route('access.store') }}",
            type: "POST",
            success: function(response) {
              table.draw()
            },
            error: function(response) {
              console.log("Error: ", response)
            }
          });

        }
      });

    });
  });
</script>


@endsection