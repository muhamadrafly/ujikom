@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Data User</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Admin Menu</div>
    <div class="breadcrumb-item">User Manage</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Posisi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="text-dark"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@include('account/modal')
<script>
  $(document).ready(function() {
    $('#Account_sidebar').parent().addClass('active');
    $('#Account_sidebar').parent().parent().parent().addClass('active');

    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('account.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum',
            searchable: false
          },
          {
            data: 'nama',
            name: 'nama'
          },
          {
            data: 'email',
            name: 'email'
          },
          {
            data: 'username',
            name: 'username'
          },
          {
            data: 'is_admin',
            name: 'is_admin'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah User");
        $('#tambah-edit-modal').modal('show');
        // $('.modal-body #Password').show();
        // $('.modal-body #Password').prev().show();
      });

      if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
          messages: {
            nama: "Nama tidak boleh kosong",
            email: {
              required: "Email tidak boleh kosong",
              email: "Alamat email salah"
            },
            username: {
              required: "Username tidak boleh kosong",
              minlength: "Username minimal 5 karakter",
              maxlength: "Username max 12 karakter"
            },
            password: {
              required: "Password tidak boleh kosong",
              minlength: "Password minimal 8 karakter"
            },
            is_admin: "Posisi tidak boleh kosong",
          },
          errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").html(error);
          },

          submitHandler: function(form) {
            $('#tombol-simpan').html('Mengirim..');

            $.ajax({
              data: $('#form-tambah-edit').serialize(),
              url: "{{ route('account.store') }}",
              type: "POST",
              dataType: 'json',
              success: function(response) {
                $('#tombol-simpan').html('Simpan');
                if (response.icon == 'error') {
                  $('#tambah-edit-modal').modal('show');
                  iziToast.error({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                } else {
                  $('#form-tambah-edit').trigger("reset");
                  $('#tambah-edit-modal').modal('hide');
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                }
              },
              error: function(response) {
                console.log('Error:', response);
                $('#tombol-simpan').html('Simpan');
              }
            });
          }
        });
      }

      $(document).on('click', '.editAccount', function() {
        $('#myModalLabel').text("Edit User");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let nama = $(this).attr("data-nama");
        let email = $(this).attr("data-email");
        let is_admin = $(this).attr("data-is_admin");
        let username = $(this).attr("data-username");
        let password = $(this).attr("data-password");

        $('.modal-body #Id').val(id);
        $('.modal-body #Nama').val(nama);
        $('.modal-body #Email').val(email);
        $('.modal-body #Is_admin').val(is_admin);
        $('.modal-body #Username').val(username);
        $('.modal-body #Password').val(password);
        // $('.modal-body #Password').prev().hide();
      });

      $(document).on('click', '.deleteAccount', function() {
        let id = $(this).attr("data-id");
        let username = $(this).attr("data-username");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data "' + username + '" akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('account.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });
      });

    });
  });
</script>

@endsection