@extends('templates/master')
@section('css')
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
@endsection
@section('content')

<div class="section-header">
    <h1>{{ $title }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">Menu</div>
        <div class="breadcrumb-item">Laporan</div>
        <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
    </div>
</div>

<div class="section-body">
    <div class="card">
        <div class="card-body">
            <div class="col-12 p-3 ml-2">
                <div class="form-group row">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-12 search-div input-group">
                        <input type="text" name="search" autocomplete="off" class="form-control search" placeholder="Cari pembelian">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fas fa-search"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group">
                        <input type="text" name="tgl_awal" autocomplete="off" class="form-control date" placeholder="Tanggal awal">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 input-group">
                        <input type="text" name="tgl_akhir" autocomplete="off" class="form-control date" placeholder="Tanggal akhir">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-12">
                        <button class="btn btn-info btn-filter" type="button"><i class="fas fa-filter"></i></button>
                        <button class="btn btn-danger btn-print" data-toggle="modal" data-target="#cetakPDF" type="button"><i class="fas fa-file-pdf"></i></button>
                        <button class="btn btn-success btn-print" data-toggle="modal" data-target="#cetakExcel" type="button"><i class="fas fa-file-excel"></i></button>
                    </div>
                </div>
            </div>

            <ul class="list-date">
                @foreach($dates as $date)
                <li class="font-weight-bold mb-2">{{ date('d M, Y', strtotime($date)) }}</li>
                @php
                $pembelian = \App\Models\Pembelian::whereDate('created_at', $date)
                ->latest()
                ->get();
                @endphp
                <div class="table-responsive">
                    <table class="table table-bordered table-stripped" style="width:100%" id="data-pembelian">

                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Kode Masuk</th>
                                <th>Tanggal Masuk</th>
                                <th>Total</th>
                                <th>Pemasok</th>
                                <th width="1%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembelian as $e=>$pem)
                            <tr>
                                <td>{{ $e+1 }}
                                    <span hidden="">{{ $pem->created_at }}</span>
                                </td>
                                <td>{{ $pem->kode_masuk }}</td>
                                <td>{{ $pem->tanggal_masuk }}</td>
                                <td>Rp. {{ number_format($pem->total) }}</td>
                                <td>{{ $pem->Pemasok->nama_pemasok}}</td>
                                <td>
                                    <button class="btn btn-sm btn-outline-info btn-selengkapnya" type="button" data-target="#dropdownPembelian{{ $pem->kode_masuk }}"><i class="fas fa-eye"></i></button>
                                </td>
                            </tr>
                            <tr id="dropdownPembelian{{ $pem->kode_masuk }}" name="dropdown" data-status="0" hidden>
                                <td colspan="6">
                                    @php
                                    $det_pem = App\Models\DetailPembelian::where('pembelian_id',$pem->id)->get();
                                    @endphp
                                    <small class="badge badge-success mt-3">Jumlah Barang : {{ $det_pem->count() }} </small>
                                    <table class="table table-sm table-borderless">
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Harga Beli</th>
                                            <th>Jumlah</th>
                                            <th>Subtotal</th>
                                        </tr>
                                        @foreach($det_pem as $e=>$det_pem)
                                        <tr>
                                            <td>{{ $e+1 }}</td>
                                            <td>{{ $det_pem->Barangs->kode_barang }}</td>
                                            <td>{{ $det_pem->Barangs->nama_barang }}</td>
                                            <td>Rp. {{ number_format($det_pem->harga_beli,0) }}</td>
                                            <td>{{ $det_pem->jumlah }}</td>
                                            <td>Rp. {{ number_format($det_pem->sub_total) }}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/datepicker.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#Laporan_pembelian_sidebar').parent().addClass('active');
        $('#Laporan_pembelian_sidebar').parent().parent().parent().addClass('active');

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('input[name=search]').on('keyup', function() {
            let keyword = $(this).val().toLowerCase();
            $(".list-date table").each(function() {
                let lineStr = $(this).text().toLowerCase();
                if (lineStr.indexOf(keyword) == -1) {
                    $(this).hide();
                    $(this).parent().prev().hide();
                } else {
                    $(this).show();
                    $(this).parent().prev().show();
                }
            });
        });

        $(document).on('click', '.btn-selengkapnya', function() {
            let target = $(this).attr('data-target');
            let status = $(target).attr('data-status');
            $(target).prop('hidden', false);
            if (status == 0) {
                $(target).fadeIn(200);
                $(this).children().removeClass('fas fa-eye');
                $(this).children().addClass('fas fa-eye-slash');
                $(target).attr('data-status', 1);
            } else {
                $(target).fadeOut(200);
                $(this).children().removeClass('fas fa-eye-slash');
                $(this).children().addClass('fas fa-eye');
                $(target).attr('data-status', 0);
            }
        });
    })
</script>
@endsection