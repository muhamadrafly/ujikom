@extends('templates/master')
@section('css')
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
@endsection
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item">Transaksi</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-5">
      <div class="card card-primary" style="margin-top:-10px;">
        <div class="card-body">

          <div class="section-title mt-0">Informasi Pembelian</div>
          <div class="form-row">
            <div class="form-group col-md-5">
              <label for="Kode_pembelian_text">Kode Pembelian <span class="text-danger">*</span></label>
              <input id="Kode_pembelian_text" type="text" class="form-control form-control-sm" readonly value="{{ $kode }}">
            </div>
            <div class="form-group col-md-7">
              <label for="Tanggal">Tanggal <span class="text-danger">*</span></label>
              <div class="input-group">
                <input id="Tanggal" type="text" autocomplete="off" class="form-control form-control-sm date" name="tanggal" tabindex="1" required>
                <button class="btn btn-sm btn-danger"><i class="fas fa-calendar-week"></i></button>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label for="Nama_pemasok" class="col-sm-4 col-form-label">Distributor <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <div class="input-group">
                <input type="text" id="Nama_pemasok" autocomplete="off" class="form-control form-control-sm" tabindex="2" required data-toggle="modal" data-target="#data-pemasok-modal">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#data-pemasok-modal"><i class="fas fa-caret-down"></i></button>
              </div>
            </div>
          </div>

          <div class="section-title mt-0">Data Barang</div>
          <div class="form-group row">
            <label for="Kode_barang" class="col-sm-4 col-form-label">Kode Barang <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <div class="input-group">
                <input type="text" readonly autocomplete="off" id="Kode_barang" class="form-control form-control-sm" tabindex="3" required data-toggle="modal" data-target="#data-barang-modal">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#data-barang-modal"><i class="fas fa-search-plus"></i></button>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="Harga_beli" class="col-sm-4 col-form-label">Harga Satuan <span class="text-danger">*</span></label>
            <div class="col-sm-8 input-group">
              <span class="btn btn-light btn-sm">Rp</span>
              <input type="number" id="Harga_beli" autocomplete="off" class="form-control form-control-sm" name="harga_beli" min="500" required>
            </div>
          </div>

          <div class="form-group row">
            <label for="Jumlah" class="col-sm-4 col-form-label">Jumlah<span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="number" id="Jumlah" autocomplete="off" class="form-control form-control-sm" name="jumlah" min="1" required>
            </div>
          </div>

          <input type="hidden" id="Id_barang">
          <input type="hidden" id="Nama_barang">

          <div class="d-flex justify-content-end">
            <button type="button" id="button-tambah-data-to-chart" class="btn btn-sm btn-primary" tabindex="4">Barang <i class="fas fa-caret-right"></i></button>
          </div>

        </div>
      </div>

    </div>
    <div class="col-md-7">
      <div class="card card-primary" style="margin-top:-10px;">

        <div class="card-body">
          <form method="post" name="form-data-barang">
            <div class="table-responsive">
              <table class="table table-sm table-custom table-hover table-striped" name="data-barang" style="width:100%">
                <thead>
                  <tr>
                    <th>Data Barang</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                  <tr>
                    <td colspan="1"></td>
                    <td><i class="text-dark d-flex justify-content-end">Tidak ada barang</i></td>
                  </tr>
                </tfoot>
              </table>
            </div>

            <div class="form-group row d-flex justify-content-end" id="Total_harga_div">
              <label for="Total_harga" class="col-sm-3 col-form-label font-weight-bold label_total_harga" hidden="">Total Harga</label>
              <div class="col-sm-4 input-group">
                <span class="form-control form-control-sm span_total_harga" hidden="">Rp.<span id="Total_harga_text"> 0</span></span>
              </div>
            </div>
            <div class="form-gorup d-flex justify-content-end">

              <input type="hidden" name="total_harga" id="Total_harga">
              <input type="hidden" name="pemasok_id" id="Pemasok_id">
              <input type="hidden" name="tanggal_masuk" id="Tanggal_masuk">
              <input type="hidden" name="kode_pembelian" id="Kode_pembelian">

              <button type="submit" class="btn btn-sm btn-success" id="btn-simpan-to-database" hidden=""><i class="fas fa-save mr-1"></i>Simpan</button>
            </div>
          </form>

        </div>
      </div>
    </div>

  </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@include('pembelian/modal')
<script>
  $(document).ready(function() {
    $('#Pembelian_sidebar').parent().addClass('active');
    $('#Pembelian_sidebar').parent().parent().parent().addClass('active');
    $('#data-barang, #data-pemasok').DataTable();

    $('.date').datepicker({
      format: 'yyyy-mm-dd'
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function iziToastWarning(title, message, position) {
      iziToast.warning({
        title: title,
        message: message,
        position: position,
      });
    }

    function tambah_barang_sama() {
      let kode_barang = $('#Kode_barang').val();
      let jumlah = $('#Jumlah').val();
      $('.kd-barang-field').each(function() {
        if ($(this).text() == kode_barang) {

          let jumlah_lama = $(this).parent().next().next().next().next().children().next().next().next().val();
          let jumlah_baru = parseInt(jumlah_lama) + parseInt(jumlah);
          $(this).parent().next().next().text(jumlah_baru);
          $(this).parent().next().next().next().next().children().next().next().next().val(jumlah_baru);

          let harga_beli = $(this).parent().next().next().next().next().children().next().next().val();
          let sub_total = parseInt(harga_beli) * parseInt(jumlah_baru);
          $(this).parent().next().next().next().text('Rp. ' + sub_total.toLocaleString());
          $(this).parent().next().next().next().next().children().next().next().next().next().val(sub_total);
          hitung_total_harga();
        }
      });
    }

    function hitung_total_harga() {
      let total_harga = 0;
      $('input[name="subtotal[]"]').each(function() {
        total_harga += parseInt($(this).val());
        $('#Total_harga_text').text(' ' + parseFloat(total_harga).toLocaleString());
        $('#Total_harga').val(total_harga);
      });
    }

    function tambah_data_to_cart(barang_id, nama_barang, kode_barang, harga_beli, jumlah, subtotal) {
      let record = '<tr><td><span class="d-block text-dark">' + nama_barang + '</span><small class="kd-barang-field text-primary">' + kode_barang + '</small></td>';
      record += '<td>Rp. ' + parseInt(harga_beli).toLocaleString() + '</td>';
      record += '<td>' + jumlah + '</td>';
      record += '<td>Rp. ' + parseInt(subtotal).toLocaleString() + '</td>';
      record += '<td><button class="btn btn-circle btn-danger btn-sm btn-hapus-field"><i class="fas fa-times"></i></button>';

      record += '<input type="hidden" name="barang_id[]" value=' + barang_id + '>';
      record += '<input type="hidden" name="harga_beli[]" value=' + harga_beli + '>';
      record += '<input type="hidden" name="jumlah[]" value=' + jumlah + '>';
      record += '<input type="hidden" name="subtotal[]" value=' + subtotal + '></td></tr>';

      $('table[name="data-barang"] tbody').append(record);
      hitung_total_harga();
    }

    $(document).on('click', '#button-tambah-data-to-chart', function(e) {
      e.preventDefault();
      if (!$('#Id_barang').val() && !$('#Kode_barang').val()) {
        iziToastWarning('Warning', 'Pilih Barang Terlebih Dahulu', 'topRight')

      } else if (!$('#Harga_beli').val()) {
        iziToastWarning('Warning', 'Harga Beli Tidak Boleh Kosong', 'topRight')

      } else if (!$('#Jumlah').val()) {
        iziToastWarning('Warning', 'Jumlah Tidak Boleh Kosong', 'topRight')

      } else {
        let id_barang = $('#Id_barang').val();
        let nama_barang = $('#Nama_barang').val();
        let kode_barang = $('#Kode_barang').val();
        let harga_beli = $('#Harga_beli').val();
        let jumlah = $('#Jumlah').val();
        let subtotal = parseFloat(harga_beli) * parseFloat(jumlah);

        let check_barang = $('.kd-barang-field:contains(' + kode_barang + ')').length;
        if (check_barang == 0) {
          tambah_data_to_cart(id_barang, nama_barang, kode_barang, harga_beli, jumlah, subtotal);
          $('#btn-simpan-to-database, .label_total_harga, .span_total_harga').prop('hidden', false);
          $('table[name="data-barang"] tfoot tr').prop('hidden', true);
        } else {
          tambah_barang_sama();
        }
        $('#Id_barang, #Kode_barang, #Nama_barang, #Harga_beli, #Jumlah').val('');
      }
    })

    $(document).on('click', '.btn-hapus-field', function() {
      $(this).parents().eq(1).remove();
      hitung_total_harga();

      if ($('.kd-barang-field').length != 0) {
        $('#btn-simpan-to-database, .label_total_harga, .span_total_harga').prop('hidden', false);
        $('table[name="data-barang"] tfoot tr').prop('hidden', true);
      } else {
        $('#btn-simpan-to-database, .label_total_harga, .span_total_harga').prop('hidden', true);
        $('table[name="data-barang"] tfoot tr').prop('hidden', false);
      }

    });

    $(document).on('click', '#btn-pilih', function() {
      if ($(this).attr('data-nama') == 'Barang') {
        $('#Id_barang').val($(this).attr('data-id'));
        $('#Kode_barang').val($(this).attr('data-kode_barang'));
        $('#Nama_barang').val($(this).attr('data-nama_barang'));
        $('#Harga_beli').val($(this).attr('data-harga_jual'));
        $('#Jumlah').val(1);
      } else {
        $('#Nama_pemasok').val($(this).attr('data-nama_pemasok'));
        $('#Pemasok_id').val($(this).attr('data-id'));
      }
      $('.btn-close').click();
    })

    $('#btn-simpan-to-database').on('click', function(e) {
      e.preventDefault();

      if (!$('input[id="Tanggal"]').val() && !$('input[id="Pemasok_id"]').val()) {
        iziToastWarning('Warning', 'Tanggal dan Toko/Distributor tidak boleh kosong', 'topRight', )

      } else if (!$('input[id="Tanggal"]').val()) {
        iziToastWarning('Warning', 'Tanggal tidak boleh kosong', 'topRight', )

      } else if (!$('input[id="Pemasok_id"]').val()) {
        iziToastWarning('Warning', 'Toko/Distributor tidak boleh kosong', 'topRight', )

      } else {
        $('#btn-simpan-to-database').html('<i class="fas fa-save mr-1"></i>Mengirim..');
        $('input[id="Tanggal_masuk"]').val($('input[id="Tanggal"]').val());
        $('input[id="Kode_pembelian"]').val($('input[id="Kode_pembelian_text"]').val());
        $.ajax({
          data: $('form[name="form-data-barang"]').serialize(),
          url: "{{ route('pembelian.store') }}",
          type: "POST",
          dataType: 'json',
          success: function(response) {
            iziToast.success({
              title: response.title,
              message: response.message,
              position: 'topRight',
            });

            $('#btn-simpan-to-database').html('<i class="fas fa-save mr-1"></i>Simpan');
            $('form[name="form-data-barang"] table tbody tr').remove();
            $('input[id="Tanggal"], input[id="Nama_pemasok"]').val('');

            $('#btn-simpan-to-database, .label_total_harga, .span_total_harga').prop('hidden', true);
            $('table[name="data-barang"] tfoot tr').prop('hidden', false);

            $('#Kode_pembelian_text').val(response.kode)
          },
          error: function(response) {
            console.log('Error:', response);
          }
        });
      }
    });

    // //--------------------Statistic Data Pembelian Jquery---------------------
    // Chart.defaults.global.defaultFontFamily = "'Nunito', 'Segoe UI', 'arial'";
    // let ctx = document.getElementById('myChart');
    // let myChart = new Chart(ctx, {
    //   type: 'line',
    //   data: {
    //     labels: [],
    //     datasets: [{
    //       label: '',
    //       data: [],
    //       borderWidth: 1,
    //     }]
    //   },
    //   options: {
    //     title: {
    //       display: true,
    //       text: '7 Data Terakhir Kenaikan & Penurunan Harga'
    //     },
    //     scales: {
    //       yAxes: [{
    //         ticks: {
    //           beginAtZero: true,
    //           callback: function(value, index, values) {
    //             if (parseInt(value) >= 1000) {
    //               return 'Rp. ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //             } else {
    //               return 'Rp. ' + value;
    //             }
    //           }
    //         }
    //       }]
    //     },
    //     legend: {
    //       display: false
    //     },
    //   }
    // });
    //
    // function ubah_data_statistic(chart, tanggal_array, harga_beli_array){
    //   chart.data = {
    //     labels: tanggal_array,
    //     datasets: [{
    //       label: '',
    //       data: harga_beli_array,
    //       borderWidth: 1
    //     }]
    //   }
    //   chart.update();
    // }
    //
    // function persentase(){
    // 	$('.persentase-status').each(function(){
    // 	  let harga_beli_awal = parseInt($(this).prev().children().first().val());
    // 	  let harga_beli_akhir = parseInt($(this).parent().prev().children().eq(2).children().first().val());
    //
    // 	  if(harga_beli_awal == 0 || harga_beli_akhir == harga_beli_awal){
    // 	    $(this).parent().prev().children().eq(3).html('<i class="fas fa-long-arrow-alt-right"></i> 0%');
    // 	    $(this).parent().prev().children().eq(3).addClass('text-primary');
    // 	    $(this).parent().prev().children().eq(4).children().first().attr('data-content', '0');
    // 	  }else if(harga_beli_akhir <script harga_beli_awal){
    // 	    let penurunan = Math.round((harga_beli_awal - harga_beli_akhir) / harga_beli_awal * 100);
    // 	    let harga_turun = harga_beli_awal - harga_beli_akhir;
    // 	    $(this).parent().prev().children().eq(3).html('<i class="fas fa-level-up-alt"></i> '+ penurunan +'%');
    // 	    $(this).parent().prev().children().eq(3).addClass('text-success');
    // 	    $(this).parent().prev().children().eq(4).children().first().attr('data-content', '+ ' + harga_turun);
    // 	  }else if(harga_beli_akhir > harga_beli_awal){
    // 	    let kenaikan = Math.round((harga_beli_akhir - harga_beli_awal) / harga_beli_awal * 100);
    // 	    let harga_naik = harga_beli_akhir - harga_beli_awal;
    // 	    $(this).parent().prev().children().eq(3).html('<i class="fas fa-level-down-alt"></i> '+ kenaikan +'%');
    // 	    $(this).parent().prev().children().eq(3).addClass('text-danger');
    // 	    $(this).parent().prev().children().eq(4).children().first().attr('data-content', '- ' + harga_naik);
    // 	  }
    // 	});
    // }
    //
    // $(document).on('click', '#btn-pilih-statistic', function() {
    //   let id = $(this).attr('data-id');
    //   $('#Kode_barang_statistic').val($(this).attr('data-kode_barang'));
    //   $.ajax({
    //     url: "{{ route('pembelian.store') }}/" + id + '/edit',
    //     type: "GET",
    //     dataType: 'json',
    //     success: function(response){
    //       $('#Nama_barang_statistic').html(response.barang.nama_barang + ' &mdash; ');
    //       $('#Stok_statistic').html('<i class="fas fa-cube"></i> ' + response.barang.stok + ' ' + response.barang.satuan + ' &mdash; ');
    //       $('#Jumlah_pemasok_statistic').html(response.jumlah_pemasok);
    //       ubah_data_statistic(myChart, response.tanggal, response.harga_beli);
    //       $.ajax({
    //         url: "{{ route('pembelian.store') }}/" + id,
    //         method: "PUT",
    //         success:function(response){
    //           $('#data-table-statistic tbody').html(response);
    //           persentase();
    //           $("[data-toggle=popover]").popover();
    //         },
    //         error: function(response) {
    //           console.log('Error:', response);
    //         }
    //       });
    //     },
    //     error: function(response) {
    //       console.log('Error:', response);
    //     }
    //   })
    //   $('.btn-close').click();
    // })
  });
</script>
@endsection