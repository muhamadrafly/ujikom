@foreach($tableRefresh as $tr)
<tr class="mb-4">
  <td>{{ date('d M, Y', strtotime($tr->created_at)) }}</td>
  @php
    $pemasok = App\Models\Pemasok::where('id', $tr->Pembelians->pemasok_id)->first();
  @endphp
  <td>{{ $pemasok->nama_pemasok }}</td>
  <td>
    <input type="hidden" name="harga_beli" value="{{ $tr->harga_beli }}">
    <a role="button" href="javascript:void(0)"></a> Rp. {{ number_format($tr->harga_beli,0,) }}
  </td>
  <td class="persentase-status"></td>
  <td>
    <a role="button" href="javascript:void(0)" class="btn btn-light btn-sm p-1" data-container="body" data-toggle="popover" data-placement="left" data-content="">
      <i class="far fa-question-circle"></i>
    </a>
  </td>
</tr>
@endforeach
<tr hidden="">
  <td></td>
  <td></td>
  <td><input type="text" name="harga_beli" value="0"></td>
  <td class="persentase-status"></td>
  <td></td>
</tr>
