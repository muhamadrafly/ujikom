@extends('templates/master')
@section('content')

<div class="section-header">
    <h1>{{ $title }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">Menu</div>
        <div class="breadcrumb-item">Transaksi</div>
        <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
    </div>
</div>


<div class="section-body">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-primary" style="margin-top:-10px;">
                <div class="card-body">

                    <div class="section-title mt-0">Informasi Penjualan</div>
                    <div class="form-group row">
                        <label for="No_faktur" class="col-sm-4 col-form-label">No Faktur</label>
                        <div class="col-sm-8">
                            <input type="text" id="No_faktur_sementara" class="form-control form-control-sm" value="{{ $kode }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Tgl_faktur" class="col-sm-4 col-form-label">Tgl Faktur</label>
                        <div class="col-sm-8 input-group">
                            <input type="text" id="Tgl_faktur_sementara" value="{{ date('Y-m-d') }}" class="form-control form-control-sm" readonly>
                            <button class="btn btn-sm btn-danger"><i class="fas fa-calendar-week"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-primary" style="margin-top:-10px;">
                <div class="card-body">
                    <div class="section-title mt-0">Informasi Pelanggan</div>
                    <div class="form-group row">
                        <label for="Kode_pelanggan" class="col-sm-3 col-form-label">Kode</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input type="text" id="Kode_pelanggan" class="form-control form-control-sm" value="M0000000" readonly data-toggle="modal" data-target="#modal-pelanggan">
                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-pelanggan"><i class="fas fa-search fa-xs"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Nama_pelanggan" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input id="Nama_pelanggan" class="form-control form-control-sm" value="Default" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-primary" style="margin-top:-10px;">
                <div class="card-body">

                    <form method="POST" id="Form-submit">
                        <table class="table table-sm table-bordered" id="data-barang-form">
                            <thead>
                                <tr>
                                    <th width="20%">Kode Barang</th>
                                    <th width="30%">Nama Barang</th>
                                    <th width="15%">Harga</th>
                                    <th width="13%">Qty</th>
                                    <th width="22%">Subtotal</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="hidden" id="Id_barang" name="barang_id[]" class="form-control form-control-sm" readonly>
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-sm kd-barang-field" autocomplete="off" id="Kode_barang" data-toggle="modal" data-target="#modal-barang">
                                            <span class="btn btn-icon btn-sm" data-toggle="modal" data-target="#modal-barang"><i class="fas fa-caret-down mt-1"></i></span>
                                        </div>
                                    </td>
                                    <td><input type="text" class="form-control form-control-sm" id="Nama_barang" readonly></td>
                                    <td>
                                        <input type="hidden" name="harga_jual[]" id="Harga_jual">
                                        <input type="text" class="form-control form-control-sm" id="Harga_jual_text" readonly>
                                    </td>
                                    <td><input type="number" class="form-control form-control-sm" id="Jumlah" name="jumlah[]" min="1" tabindex="1" readonly autocomplete="off">
                                    </td>
                                    <td>
                                        <input type="hidden" name="sub_total[]" id="Subtotal">
                                        <input type="text" class="form-control form-control-sm" readonly id="Subtotal_text">
                                    </td>
                                    <td><button type="button" id="btn-hapus" class="btn btn-circle btn-danger btn-sm btn-hapus-field"><i class="fas fa-times"></i></button></td>
                                </tr>
                            </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="3"><button type="button" class="btn btn-sm btn-primary mt-1" id="Baris-baru"><i class="fas fa-plus-circle mr-1"></i>Baris Baru</button></td>
                                    <td>
                                        <h6 class="mt-2">Total</h6>
                                    </td>
                                    <td colspan="2">
                                        <input type="hidden" id="Total_bayar" name="total_bayar">
                                        <input type="text" class="form-control form-control-sm" id="Total_bayar_text" readonly>
                                    </td>
                                </tr>
                            </tfoot> -->
                        </table>

                        <div class="alert alert-light">
                            <div class="row" style="margin-bottom: -25px;">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-sm btn-primary" id="Baris-baru"><i class="fas fa-plus-circle mr-1"></i>Baris Baru</button>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row d-flex justify-content-end">
                                        <label for="Bayar" class="col-sm-4 col-form-label">Total Bayar</label>
                                        <div class="col-sm-7">
                                            <input type="hidden" id="Total_bayar" name="total_bayar">
                                            <span type="text" class="form-control form-control-sm" id="Total_bayar_text"></span>
                                            <!-- <input type="text" class="form-control form-control-sm" id="Total_bayar_text"> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Hidden Input -->
                        <input type="hidden" name="no_faktur" id="No_faktur">
                        <input type="hidden" name="tgl_faktur" id="Tgl_faktur">
                        <input type="hidden" name="pelanggan_id" id="Pelanggan_id" value="default">

                        <div class="row">
                            <div class="col-md-6">
                                <textarea id="Catatan" type="text" class="form-control" name="catatan" placeholder="Catatan"></textarea>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row d-flex justify-content-end">
                                    <label for="Bayar" class="col-sm-3 col-form-label">Bayar</label>
                                    <div class="col-sm-7">
                                        <input type="number" tabindex="10" id="Bayar" name="bayar" class="form-control form-control-sm" placeholder="0">
                                    </div>
                                </div>
                                <div class="form-group row d-flex justify-content-end">
                                    <label for="Kembali" class="col-sm-3 col-form-label">Kembali</label>
                                    <div class="col-sm-7">
                                        <input type="hidden" id="Kembali" name="kembali" value="0">
                                        <input type="number" id="Kembali_text" class="form-control form-control-sm" placeholder="0" readonly>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="submit" tabindex="11" class="btn btn-success btn-proses-pembayaran"><i class="fas fa-cash-register mr-1"></i>Proses Pembayaran</button>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('scripts')
@include('penjualan/modal')
<script>
    $(document).ready(function() {
        $('#Penjualan_sidebar').parent().addClass('active');
        $('#Penjualan_sidebar').parent().parent().parent().addClass('active');
        $('#data-barang, #data-pelanggan').DataTable();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function iziToastWarning(title, message, position) {
            iziToast.warning({
                title: title,
                message: message,
                position: position,
            });
        }

        let tabindex = 1;

        function add_rows() {
            tabindex += 1;
            let Rows = '<tr>';
            Rows += '<td>';
            Rows += '<input type="hidden" id="Id_barang" name="barang_id[]" class="form-control form-control-sm" readonly>';
            Rows += '<div class="input-group">';
            Rows += '<input type="text" class="form-control form-control-sm kd-barang-field" autocomplete="off" id="Kode_barang" data-toggle="modal" data-target="#modal-barang">';
            Rows += '<span class="btn btn-icon btn-sm" data-toggle="modal" data-target="#modal-barang"><i class="fas fa-caret-down mt-1"></i></span>';
            Rows += '</div>';
            Rows += '</td>';
            Rows += '<td><input type="text" class="form-control form-control-sm" id="Nama_barang" readonly></td>';
            Rows += '<td>';
            Rows += '<input type="hidden" name="harga_jual[]" id="Harga_jual">';
            Rows += '<input type="text" class="form-control form-control-sm" id="Harga_jual_text" readonly>';
            Rows += '</td>';
            Rows += '<td><input type="number" class="form-control form-control-sm" id="Jumlah" name="jumlah[]" tabindex="' + tabindex + '" min="1" readonly autocomplete="off">';
            Rows += '</td>';
            Rows += '<td>';
            Rows += '<input type="hidden" name="sub_total[]" value="0" id="Subtotal">';
            Rows += '<input type="text" class="form-control form-control-sm" readonly id="Subtotal_text">';
            Rows += '</td>';
            Rows += '<td><button id="btn-hapus" type="button" class="btn btn-circle btn-danger btn-sm"><i class="fas fa-times"></i></button></td>';
            Rows += '</tr>';

            $('table[id="data-barang-form"] tbody').append(Rows);
        }

        let warning_rows = 0;
        $(document).on('click', '#btn-pilih', function() {
            if ($(this).attr('data-nama') == 'Barang') {
                let barang_id = $(this).attr('data-id');
                let kode_barang = $(this).attr('data-kode_barang');
                let nama_barang = $(this).attr('data-nama_barang');
                let harga_jual = $(this).attr('data-harga_jual');

                $('.kd-barang-field').each(function() {
                    if ($(this).val() == kode_barang) {
                        iziToastWarning('Proces Fail', 'Barang Sudah Ada', 'topRight')
                        return false;
                    } else if (!$(this).val()) {
                        warning_rows = 1;
                        $(this).parent().parent().next().next().next().children().prop('readonly', false);
                        $(this).prop('readonly', true);
                        $(this).attr('data-toggle', ' ');
                        $(this).next().attr('data-toggle', ' ');
                        $(this).parent().prev().val(barang_id);
                        $(this).val(kode_barang);
                        $(this).parent().parent().next().children().val(nama_barang);
                        $(this).parent().parent().next().next().children().val(harga_jual);
                        $(this).parent().parent().next().next().children().next().val(parseInt(harga_jual).toLocaleString());
                        $(this).parent().parent().next().next().next().children().val(1);
                        $(this).parent().parent().next().next().next().next().children().val(harga_jual);
                        $(this).parent().parent().next().next().next().next().children().next().val(parseInt(harga_jual).toLocaleString());
                        $(this).parent().parent().next().next().next().next().next().children().addClass('btn-hapus-field');
                    }
                })
                hitung_total_bayar()
            } else {
                $('#Pelanggan_id').val($(this).attr('data-id'));
                $('#Kode_pelanggan').val($(this).attr('data-kode_pelanggan'));
                $('#Nama_pelanggan').val($(this).attr('data-nama'));
            }
            $('.btn-close').click();
        })

        $(document).on('keyup change', '#Jumlah', function() {
            let jumlah = $(this).val();
            let harga_jual = $(this).parent().prev().children().val();
            let sub_total = parseInt(jumlah) * parseInt(harga_jual);
            $(this).parent().next().children().val(sub_total);
            $(this).parent().next().children().next().val(parseInt(sub_total).toLocaleString());

            let sub_total_val = $(this).parent().next().children().val();
            if (isNaN(sub_total_val)) {
                if (harga_jual != '') {
                    $(this).parent().next().children().val(harga_jual);
                    $(this).parent().next().children().next().val(parseInt(harga_jual).toLocaleString());
                } else {
                    $(this).parent().next().children().val(0);
                    $(this).parent().next().children().next().val(0);
                }
            }
            hitung_total_bayar()
        })

        function kembali(jml) {
            let total_bayar = $('#Total_bayar').val();
            if (jml != '') {
                let kembali = parseInt(jml) - parseInt(total_bayar);
                $('#Kembali').val(kembali);
                $('#Kembali_text').val(parseInt(kembali).toLocaleString());
            }
        }

        function hitung_total_bayar() {
            let total_bayar = 0;
            $('#data-barang-form #Subtotal').each(function() {
                if ($(this).val() != '') {
                    total_bayar += parseInt($(this).val());
                    $('#Total_bayar').val(total_bayar);
                    $('#Total_bayar_text').text(parseInt(total_bayar).toLocaleString());
                }
            })
            kembali($('#Bayar').val())
        }

        $(document).on('click', '.btn-hapus-field', function() {
            if ($('.kd-barang-field').length == 1) {
                $('#Jumlah').prop('readonly', true);
                $('.kd-barang-field').prop('readonly', false);
                $('.kd-barang-field').attr('data-toggle', "modal");
                $('.kd-barang-field').next().attr('data-toggle', "modal");
                $('#Id_barang, #Kode_barang, #Nama_barang, #Harga_jual, #Harga_jual_text, #Jumlah, #Subtotal, #Subtotal_text, #Total_bayar').val('');
                $('#Total_bayar_text').text('');
            } else {
                $(this).parents().eq(1).remove();
                hitung_total_bayar();
            }
        });

        $(document).on('click', '#Baris-baru', function() {
            if (warning_rows == 1) {
                add_rows()
                warning_rows = 0;
            } else {
                iziToastWarning('Add Rows Fail', 'Isi Baris Kosong Terlebih Dahulu', 'topRight')
            }
        })

        $('#Bayar').on('keyup change', function() {
            kembali($(this).val());
        })
        // else if ($('#Total_bayar').val() > $('#Bayar').val()) {
        //     iziToastWarning('Proces Fail', 'Jumlah Bayar Kurang', 'topRight')
        // }

        $(document).on('submit', '#Form-submit', function(e) {
            e.preventDefault();
            if (!$('#Id_barang').val() && !$('#Kode_barang').val()) {
                iziToastWarning('Proces Fail', 'Pilih Barang Terlebih Dahulu', 'topRight')
            } else if (!$('#Bayar').val()) {
                iziToastWarning('Proces Fail', 'Jumlah Bayar Tidak Boleh Kosong', 'topRight')
            } else {
                $('.btn-proses-pembayaran').html('<i class="fas fa-cash-register mr-1"></i>Mengirim..');
                $('.btn-proses-pembayaran').prop('disable', true);
                $('#No_faktur').val($('#No_faktur_sementara').val());
                $('#Tgl_faktur').val($('#Tgl_faktur_sementara').val());
                let Request = new FormData(this);
                $.ajax({
                    data: Request,
                    url: "{{ route('penjualan.store') }}",
                    type: "POST",
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType: 'json',
                    success: function(response) {
                        $('.btn-proses-pembayaran').html('<i class="fas fa-cash-register mr-1"></i>Proses Pembayaran');
                        $('.btn-proses-pembayaran').prop('disable', false);
                        $('#No_faktur').val(response.kode);
                        $('#No_faktur_sementara').val(response.kode)
                        iziToast.success({
                            title: response.title,
                            message: response.message,
                            position: 'topRight',
                        });
                        $(':button[id="btn-hapus"]').click();
                        $('#Bayar, #Kembali, #Kembali_text, #Total_bayar, #Catatan').val('');
                        $('#Total_bayar_text').text('');
                        $('#Kode_pelanggan').val('M0000000');
                        $('#Nama_pelanggan').val('Default');
                        $('#Pelanggan_id').val('default');
                    },
                })
            }
        })

    })
</script>

@endsection