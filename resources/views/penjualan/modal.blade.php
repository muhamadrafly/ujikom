<div class="modal fade" id="modal-barang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Barang</h5>
                <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-sm" id="data-barang" width="100%">
                        <thead class="text-dark bg-light">
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                <th>Produk</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Stok</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark">
                            @foreach(App\Models\Barang::orderBy('kode_barang')->where('stok', '!=', 0)->get() as $e=>$brg)
                            <tr>
                                <td>{{ $e+1 }}</td>
                                <td>{{ $brg->kode_barang }}</td>
                                <td>{{ $brg->Produks->nama_produk }}</td>
                                <td>{{ $brg->nama_barang }}</td>
                                <td>{{ $brg->satuan }}</td>
                                <td>Rp. {{ number_format($brg->harga_jual, 0) }}</td>
                                <td>{{ $brg->stok }}</td>
                                <td>
                                    <button type="button" class="btn btn-icon btn-sm btn-primary" id="btn-pilih" data-nama="Barang" data-id="{{ $brg->id }}" data-kode_barang="{{ $brg->kode_barang }}" data-nama_barang="{{ $brg->nama_barang }}" data-harga_jual="{{ $brg->harga_jual }}"><i class=" fas fa-check-double"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pelanggan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <!-- style="max-width: 90%; margin:auto; margin-top:15px;" -->
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Pelanggan</h5>
                <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-sm" id="data-pelanggan" width="100%">
                        <thead class="text-dark bg-light">
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody class="text-dark">
                            @php
                            $pelanggan = App\Models\Pelanggan::orderBy('kode_pelanggan')->where('id', '!=', 'default')->get();
                            @endphp
                            @foreach($pelanggan as $e=>$plng)
                            <tr>
                                <td>{{ $e+1 }}</td>
                                <td>{{ $plng->kode_pelanggan }}</td>
                                <td>{{ $plng->nama }}</td>
                                <td>{{ $plng->alamat }}</td>
                                <td>{{ $plng->email }}</td>
                                <td>
                                    <button type="button" class="btn btn-icon btn-sm btn-primary" id="btn-pilih" data-id="{{ $plng->id }}" data-nama="{{ $plng->nama }}" data-kode_pelanggan="{{ $plng->kode_pelanggan }}"><i class="fas fa-check-double"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>