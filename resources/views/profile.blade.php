@extends('templates/master')
@section('content')

<div class="section-header">
    <h1>{{ $title }}</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">Menu</div>
        <div class="breadcrumb-item"><a href="javascript:void(0)">Profile</a></div>
    </div>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-md-5">
            <div class="card author-box card-primary">
                <div class="card-body">
                    <div class="author-box-left">
                        <img alt="image" src="{{ asset('img/default_profile.jpg') }}" class="rounded-circle author-box-picture">
                    </div>
                    <div class="author-box-details">
                        <div class="author-box-name">
                            <a href="javascript:void(0)">{{ Auth::user()->nama }}</a>
                        </div>
                        <div class="author-box-job">
                            @if(Auth::user()->is_admin == 1)
                            <h6>Administrator</h6>
                            @else
                            <h6>Cashier</h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card card-primary">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Ubah Data Diri</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Ubah Password</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade active show" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <form id="data-user" method="post">
                                <div class="form-group">
                                    <label for="Nama">Nama <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="Nama" name="nama" value="{{ Auth::user()->nama }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="Email">Email <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="Email" name="email" value="{{ Auth::user()->email }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="Usernamae">Username <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="Usernamae" name="username" value="{{ Auth::user()->username }}" required>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary" id="tombol-simpan-data"><i class="fas fa-file mr-1"></i>Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <form id="data-password" method="post">
                                <div class="form-group">
                                    <label for="Password_lama">Password Lama <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="Password_lama" name="password_lama" tabindex="1" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="Password_baru">Password Baru <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="Password_baru" name="password_baru" tabindex="1" required>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary" id="tombol-simpan-password"><i class="fas fa-file mr-1"></i>Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="{{ asset('js/jquery.form-validator.min.js')}}"></script>
@section('scripts')

<script>
    $(document).ready(function() {

        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#data-user').on('submit', function(e) {
                e.preventDefault();
                $('#tombol-simpan-data').html('Mengirim...');
                $.ajax({
                    data: $('#data-user').serialize(),
                    url: "{{ route('profile.store') }}",
                    type: "POST",
                    dataType: 'json',

                    success: function(response) {
                        $('#tombol-simpan-data').html('<i class="fas fa-file mr-1"></i>Simpan Perubahan');
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            icon: response.icon,
                        })
                    },
                    error: function(response) {
                        console.log('Error:', response);
                        $('#tombol-simpan-data').html('<i class="fas fa-file mr-1"></i>Simpan Perubahan');
                    }
                })
            })

            $('#data-password').on('submit', function(e) {
                e.preventDefault();
                $('#tombol-simpan-password').html('Mengirim...');
                $.ajax({
                    data: $('#data-password').serialize(),
                    url: "{{ route('profile.store') }}/{{ Auth::id() }}",
                    type: "PUT",
                    dataType: 'json',

                    success: function(response) {
                        $('#data-password :input').val('');
                        $('#tombol-simpan-password').html('<i class="fas fa-file mr-1"></i>Simpan Perubahan');
                        Swal.fire({
                            title: response.title,
                            text: response.message,
                            icon: response.icon,
                        })
                    },
                    error: function(response) {
                        console.log('Error:', response);
                        $('#tombol-simpan-password').html('<i class="fas fa-file mr-1"></i>Simpan Perubahan');
                    }
                })
            })

        });
    });
</script>


@endsection