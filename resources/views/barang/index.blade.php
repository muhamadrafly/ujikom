@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Data Barang</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item">Data Master</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Produk</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody class="text-dark"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@include('barang/modal')
<script>
  $(document).ready(function() {
    $('#Barang_sidebar').parent().addClass('active');
    $('#Barang_sidebar').parent().parent().parent().addClass('active');

    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('barang.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum'
          }, {
            data: 'kode_barang',
            name: 'kode_barang'
          },
          {
            data: 'produk_id',
            name: 'produk_id'
          },
          {
            data: 'nama_barang',
            name: 'nama_barang'
          },
          {
            data: 'satuan',
            name: 'satuan'
          },
          {
            data: 'harga_jual',
            name: 'harga_jual'
          },
          {
            data: 'stok',
            name: 'stok'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        // let foo = <?= json_encode($lastId = App\Models\Barang::select('kode_barang')->orderBy('kode_barang', 'desc')->first()) ?>;
        // let kode_barang = <?= json_encode(($lastId == null ? 'B0000001' : sprintf('B%07d', substr($lastId->kode_barang, 1) + 1))) ?>;
        // alert(kode_barang);
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah Barang");
        $('#tambah-edit-modal').modal('show');
      });

      if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
          messages: {
            produk_id: "Produk tidak boleh kosong",
            nama_barang: "Nama barang tidak boleh kosong",
            satuan: "Satuan tidak boleh kosong",
            stok: "Stok tidak boleh kosong",
            harga_jual: {
              required: "Harga tidak boleh kosong",
              min: "Harga minimal Rp.500",
            },
          },
          errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").html(error);
          },

          submitHandler: function(form) {
            $('#tombol-simpan').html('Mengirim..');

            $.ajax({
              data: $('#form-tambah-edit').serialize(),
              url: "{{ route('barang.store') }}",
              type: "POST",
              dataType: 'json',
              success: function(response) {
                $('#tombol-simpan').html('Simpan');
                if (response.icon == 'error') {
                  $('#tambah-edit-modal').modal('show');
                  iziToast.error({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                } else {
                  $('#form-tambah-edit').trigger("reset");
                  $('#tambah-edit-modal').modal('hide');
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                }
              },
              error: function(response) {
                console.log('Error:', response);
                $('#tombol-simpan').html('Simpan');
              }
            });
          }
        });
      }

      $(document).on('click', '.editBarang', function() {
        $('#myModalLabel').text("Edit Barang");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let kode_barang = $(this).attr("data-kode_barang");
        let produk_id = $(this).attr("data-produk_id");
        let nama_barang = $(this).attr("data-nama_barang");
        let satuan = $(this).attr("data-satuan");
        let harga_jual = $(this).attr("data-harga_jual");
        let stok = $(this).attr("data-stok");

        $('.modal-body #Id').val(id);
        $('.modal-body #kode_barang').val(kode_barang);
        $('.modal-body #Produk_id').val(produk_id);
        $('.modal-body #Nama_barang').val(nama_barang);
        $('.modal-body #Satuan').val(satuan);
        $('.modal-body #Harga_jual').val(harga_jual);
        $('.modal-body #Stok').val(stok);
      });

      $(document).on('click', '.deleteBarang', function() {
        let id = $(this).attr("data-id");
        let nama_barang = $(this).attr("data-nama_barang");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data "' + nama_barang + '" akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('barang.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                  $('#tombol-simpan').html('Simpan');
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });

      });

    });
  });
</script>

@endsection