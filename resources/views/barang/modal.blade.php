<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <input type="hidden" name="id" id="Id" value="">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="kode_barang">Kode Barang</label>
              <input id="kode_barang" type="text" class="form-control" value="{{ $kode }}" name="kode_barang" tabindex="1" readonly>
            </div>
            <div class="form-group col-md-8">
              <label for="Produk_id">Nama Produk <span class="text-danger">*</span> </label>
              @php $produk = \App\Models\Produk::get() @endphp
              <select id="Produk_id" type="text" class="form-control custom-select" name="produk_id" maxlength="50" tabindex="1" required>
                <option value="">Pilih..</option>
                @foreach($produk as $pd)
                <option value="{{ $pd->id }}">{{ $pd->nama_produk }}</option>
                @endforeach
              </select>
              <div class="invalid-feedback" id="produk_id_error"></div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-7">
              <label for="nama_barang">Nama Barang <span class="text-danger">*</span></label>
              <input id="Nama_barang" type="text" class="form-control" name="nama_barang" tabindex="2" required>
              <div class="invalid-feedback" id="nama_barang_error"></div>
            </div>
            <div class="form-group col-md-5">
              <label for="Satuan">Satuan <span class="text-danger">*</span></label>
              <input id="Satuan" class="form-control" name="satuan" tabindex="3" required>
              <div class="invalid-feedback" id="satuan_error"></div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-7">
              <label for="Harga_jual">Harga jual <span class="text-danger">*</span></label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    Rp
                  </div>
                </div>
                <input type="number" class="form-control" id="Harga_jual" name="harga_jual" autocomplete="off" min="500" tabindex="4" required>
                <div class="invalid-feedback" id="harga_jual_error" style="margin-left: 55px;"></div>
              </div>
            </div>

            <div class="form-group col-md-5">
              <label for="Stok">Stok <span class="text-danger">*</span></label>
              <input type="number" class="form-control" name="stok" id="Stok" min="1" autocomplete="off" tabindex="5" required>
              <div class="invalid-feedback" id="stok_error"></div>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="6">Simpan</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>