<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <input type="hidden" name="id" id="Id" value="">
          <div class="form-group">
            <label for="barang_id">Kode Barang <span class="text-danger">*</span></label>
            <select name="barang_id" class="form-control" id="Barang_id" required>
              <option value="">Pilih..</option>
              @foreach(App\Models\Barang::orderBy('kode_barang')->get() as $br)
              <option value="{{ $br->id }}">{{ $br->kode_barang }} - {{ $br->nama_barang }}</option>
              @endforeach
            </select>
            <div class="invalid-feedback" id="barang_id_error"></div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="nama_barang">Produk</label>
              <input type="text" class="form-control" readonly id="Nama_barang">
            </div>
            <div class="form-group col-md-6">
              <label for="nama_barang">Stok</label>
              <input type="text" class="form-control" readonly id="Stok">
            </div>
          </div>
          <div class="form-group">
            <label for="Tanggal_expired">Tanggal Expired <span class="text-danger">*</span></label>
            <input type="date" class="form-control" name="tanggal_expired" id="Tanggal_expired" autocomplete="off" required>
            <div class="invalid-feedback" id="tanggal_expired_error"></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="2">Simpan</button>
        <button type="submit" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>