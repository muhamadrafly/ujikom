@extends('templates/master')
@section('css')
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
@endsection
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Pengajuan</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="card">
    <div class="card-body">
      <a href="{{ url('menu/penarikan_barang') }}/{{ Auth::id() }}/edit" class="btn btn-sm btn-danger mb-2"><i class="far fa-file-pdf mr-1"></i>PDF</a>
      <a href="{{ url('menu/penarikan_barang/create') }}" class="btn btn-sm btn-success mb-2"><i class="far fa-file-excel mr-1"></i>Excel</a>
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
          <thead class="text-dark bg-light">
            <tr>
              <th width="5%">No</th>
              <th>Kode Barang</th>
              <th>Nama Barang</th>
              <th>Tanggal Expired</th>
              <th>Stok</th>
              <th>Ditarik</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody class="text-dark"></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@include('penarikan_barang/modal')
<script>
  $(document).ready(function() {
    $('#Penarikan_barang_sidebar').parent().addClass('active');
    $('#Penarikan_barang_sidebar').parent().parent().parent().addClass('active');

    $('.date').datepicker({
      format: 'yyyy-mm-dd'
    });

    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('penarikan_barang.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum',
            searchable: false
          },
          {
            data: 'kode_barang',
            name: 'kode_barang'
          },
          {
            data: 'nama_barang',
            name: 'nama_barang'
          },
          {
            data: 'tanggal_expired',
            name: 'tanggal_expired'
          },
          {
            data: 'stok',
            name: 'stok'
          },
          {
            data: 'ditarik',
            name: 'ditarik'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#Barang_id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah Pengajuan");
        $('#tambah-edit-modal').modal('show');
      });

      if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
          messages: {
            barang_id: "barang_id tidak boleh kosong",
            tanggal_expired: "tanggal_expired tidak boleh kosong",
          },
          errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").html(error);
          },

          submitHandler: function(form) {
            $('#tombol-simpan').html('Mengirim..');
            let dataPenarikan = new FormData(form);
            $.ajax({
              data: dataPenarikan,
              url: "{{ route('penarikan_barang.store') }}",
              type: "POST",
              dataType: 'json',
              contentType: false,
              cache: false,
              processData: false,
              success: function(response) {
                $('#tombol-simpan').html('Simpan');
                if (response.icon == 'error') {
                  $('#tambah-edit-modal').modal('show');
                  iziToast.error({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                } else {
                  $('#form-tambah-edit').trigger("reset");
                  $('#tambah-edit-modal').modal('hide');
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                }
              },
              error: function(response) {
                console.log('Error:', response);
                $('#tombol-simpan').html('Simpan');
              }
            });
          }
        });
      }

      // $(document).on('click', 'input[name="ditarik_status"]', function() {
      //   let data_ditarik = {
      //     barang_id: $(this).attr('data-barang_id'),
      //     ditarik: $(this).attr('data-ditarik')
      //   }
      //   iziToast.success({
      //     title: 'Change Success',
      //     message: 'Status Ditarik Berhasil Diubah',
      //     position: 'topRight',
      //   });
      //   $.ajax({
      //     data: data_ditarik,
      //     url: "{{ route('penarikan_barang.store') }}/{{ Auth::id() }}",
      //     method: "PUT",
      //     dataType: "json",
      //     success: function(response) {
      //       table.draw();
      //     },
      //     error: function(response) {
      //       console.log("Error: ", response)
      //     }
      //   })
      //   alert(data);

      // })

      $('#Barang_id').on('change', function() {
        barang_searching($(this).val());
      })

      function barang_searching(id) {
        $.ajax({
          url: "{{ route('penarikan_barang.store') }}/" + id,
          type: "GET",
          dataType: 'json',
          success: function(response) {
            $('#Nama_barang').val(response.produk);
            $('#Stok').val(response.barang.stok);
          },
          error: function(response) {
            alert('error')
          }
        });
      }

      $(document).on('click', '.editPenarikanBarang', function() {
        $('#myModalLabel').text("Edit Penarikan Barang");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let barang_id = $(this).attr("data-barang_id");
        let tanggal_expired = $(this).attr("data-tanggal_expired");
        barang_searching(barang_id)
        $('.modal-body #Id').val(id);
        $('.modal-body #Barang_id').val(barang_id);
        $('.modal-body #Tanggal_expired').val(tanggal_expired);

      });

      $(document).on('click', '.deletePenarikanBarang', function() {
        let id = $(this).attr("data-id");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('penarikan_barang.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                  $('#tombol-simpan').html('Simpan');
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });
      });

    });
  });
</script>

@endsection