<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		html {
			font-family: "Arial", sans-serif;
			margin: 0;
			padding: 0;
		}

		.header {
			background-color: #d3eafc;
			padding: 40px 70px;
		}

		.body {
			padding: 20px 70px;
		}

		/* Text */
		.text-30 {
			font-size: 30px;
		}

		.text-20 {
			font-size: 20px;
		}

		.text-18 {
			font-size: 18px;
		}

		.text-16 {
			font-size: 16px;
		}

		.text-14 {
			font-size: 14px;
		}

		.text-12 {
			font-size: 12px;
		}

		.text-10 {
			font-size: 10px;
		}

		.font-bold {
			font-weight: bold;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.text-center {
			text-align: center;
			margin-right: 50px;
		}

		.txt-dark {
			color: #5b5b5b;
		}

		.txt-dark2 {
			color: #1d1d1d;
		}

		.txt-blue {
			color: #2a4df1;
		}

		.txt-light {
			color: #acacac;
		}

		.txt-green {
			color: #19d895;
		}

		p {
			margin: 0;
		}

		.d-block {
			display: block;
		}

		.w-100 {
			width: 100%;
		}

		.mt-2 {
			margin-top: 10px;
		}

		.mb-1 {
			margin-bottom: 5px;
		}

		.mb-4 {
			margin-bottom: 20px;
		}

		.pt-30 {
			padding-top: 30px;
		}

		.pt-15 {
			padding-top: 15px;
		}

		.pt-5 {
			padding-top: 5px;
		}

		.pb-5 {
			padding-bottom: 5px;
		}

		table {
			border-collapse: collapse;
		}

		thead tr td {
			border-bottom: 0.5px solid #d9dbe4;
			color: #7e94f6;
			font-size: 12px;
			padding: 5px;
			text-transform: uppercase;
		}

		tbody tr td {
			padding: 7px;
		}

		.border-top-foot {
			border-top: 0.5px solid #d9dbe4;
		}

		.mr-20 {
			margin-right: 20px;
		}

		ul {
			padding: 0;
		}

		ul li {
			list-style-type: none;
		}

		.w-300p {
			width: 300px;
		}
	</style>
</head>

<body>
	<div class="header">
		<table class="w-100">
			<tr>
				<td class="text-center text-20 txt-blue font-bold">LAPORAN DATA PENARIKAN BARANG</td>
				<td class="text-right text-12 txt-blue">Oleh {{ Auth::user()->nama }}</td>
			</tr>
		</table>
	</div>

	<div class="body">
		<table class="w-100 mb-4">
			<thead>
				<tr>
					<td>Kode Barang</td>
					<td>Nama Barang</td>
					<td>Tanggal Expired</td>
					<td>Stok</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $dt)
				<tr>
					<td class="txt-dark text-10">{{ $dt->Barangs->kode_barang }}</td>
					<td class="txt-dark text-10">{{ $dt->Barangs->nama_barang }}</td>
					<td class="txt-dark text-10">{{ $dt->tanggal_expired }}</td>
					<td class="txt-dark text-10">{{ $dt->Barangs->stok }}</td>
					@if($dt->ditarik == 1)
					<td class="txt-dark text-10">DITARIK</td>
					@else
					<td class="txt-dark text-10">TIDAK DITARIK</td>
					@endif
				</tr>
				@endforeach

			</tbody>
		</table>
</body>

</html>