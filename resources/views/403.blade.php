<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <link rel="icon" href="{{ asset('img/logo.jpg') }}">
  <title>Em &mdash; Error</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <link href="{{ asset('css/components.css') }}" rel="stylesheet">

</head>

<body class="sidebar-gone">
  <div id="app">
    <section class="section">
      <div class="container">
        <div class="page-error">
          <div class="page-inner">
            <h1>403</h1>
            <div class="page-description">
              Anda tidak memiliki akses pada halaman ini.
            </div>
            <div class="page-search">
              <div class="mt-3">
                <a href="{{ url('menu/home') }}">Kembali ke Home</a>
              </div>
            </div>
          </div>
        </div>
        <div class="simple-footer">
          Copyright © {{ date('Y') }}
          <div class="bullet"></div> ElMarket
        </div>

      </div>
    </section>
  </div>

  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/stisla.js') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>

</body>

</html>