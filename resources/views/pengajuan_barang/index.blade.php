@extends('templates/master')
@section('css')
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}">
@endsection
@section('content')

<div class="section-header">
 <h1>{{ $title }}</h1>
 <div class="section-header-button">
  <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Pengajuan Barang</a>
 </div>
 <div class="section-header-breadcrumb">
  <div class="breadcrumb-item active">Menu</div>
  <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
 </div>
</div>

<div class="section-body">
 <div class="card">
  <div class="card-body">
  <a href="{{ url('menu/pengajuan_barang') }}/{{ Auth::id() }}/edit" class="btn btn-sm btn-danger mb-2"><i class="far fa-file-pdf mr-1"></i>PDF</a>
  <a href="{{ url('menu/pengajuan_barang/create') }}" class="btn btn-sm btn-success mb-2"><i class="far fa-file-excel mr-1"></i>Excel</a>
   <div class="table-responsive">
    <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
     <thead class="text-dark bg-light">
      <tr>
       <th width="5%">No</th>
       <th>Nama Pengaju</th>
       <th>Nama Barang</th>
       <th>Tanggal Pengajuan</th>
       <th>Qty</th>
       <th>Terpenuhi</th>
       <th width="10%">Action</th>
      </tr>
     </thead>
     <tbody class="text-dark"></tbody>
    </table>
   </div>
  </div>
 </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/datepicker.min.js') }}"></script>
@include('pengajuan_barang/modal')
<script>
 $(document).ready(function() {
  $('#Pengajuan_barang_sidebar').parent().addClass('active');
  $('#Pengajuan_barang_sidebar').parent().parent().parent().addClass('active');

  $('.date').datepicker({
   format: 'yyyy-mm-dd'
  });

  $(function() {
   $.ajaxSetup({
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
   });
   let table = $('#data-table').DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: "{{ route('pengajuan_barang.index') }}",
    columns: [{
      data: 'rownum',
      name: 'rownum',
      searchable: false
     },
     {
      data: 'pelanggan_id',
      name: 'pelanggan_id'
     },
     {
      data: 'nama_barang',
      name: 'nama_barang'
     },
     {
      data: 'tanggal_pengajuan',
      name: 'tanggal_pengajuan'
     },
     {
      data: 'qty',
      name: 'qty'
     },
     {
      data: 'terpenuhi',
      name: 'terpenuhi'
     },
     {
      data: 'action',
      name: 'action',
      orderable: false,
      searchable: false
     },
    ],
   });

   $('#tombol-tambah').click(function() {
    $('#button-simpan').val("create-post");
    $('#Id').val('');
    $('#form-tambah-edit').trigger("reset");
    $('#myModalLabel').text("Tambah Pengajuan Barang");
    $('#tambah-edit-modal').modal('show');
   });

   if ($("#form-tambah-edit").length > 0) {
    $("#form-tambah-edit").validate({
     messages: {
      pelanggan_id: "Nama pengaju tidak boleh kosong",
      nama_barang: "Nama barang tidak boleh kosong",
      tanggal_pengajuan: "Tanggal pengajuan tidak boleh kosong",
      qty: "Qty tidak boleh kosong",
     },
     errorPlacement: function(error, element) {
      let name = element.attr("name");
      $("#" + name + "_error").html(error);
     },

     submitHandler: function(form) {
      $('#tombol-simpan').html('Mengirim..');
      let dataPenarikan = new FormData(form);
      $.ajax({
       data: dataPenarikan,
       url: "{{ route('pengajuan_barang.store') }}",
       type: "POST",
       dataType: 'json',
       contentType: false,
       cache: false,
       processData: false,
       success: function(response) {
        $('#tombol-simpan').html('Simpan');
        if (response.icon == 'error') {
         $('#tambah-edit-modal').modal('show');
         iziToast.error({
          title: response.title,
          message: response.message,
          position: 'topRight',
         });
        } else {
         $('#form-tambah-edit').trigger("reset");
         $('#tambah-edit-modal').modal('hide');
         table.draw();
         iziToast.success({
          title: response.title,
          message: response.message,
          position: 'topRight',
         });
        }
       },
       error: function(response) {
        console.log('Error:', response);
        $('#tombol-simpan').html('Simpan');
       }
      });
     }
    });
   }

   $(document).on('click', 'input[name="terpenuhi_status"]', function() {
    let data_terpenuhi = {
     id: $(this).attr('data-id'),
     terpenuhi: $(this).attr('data-terpenuhi')
    }

    iziToast.success({
     title: 'Change Success',
     message: 'Status terpenuhi Berhasil Diubah',
     position: 'topRight',
    });
    $.ajax({
     data: data_terpenuhi,
     url: "{{ route('pengajuan_barang.store') }}/{{ Auth::id() }}",
     method: "PUT",
     dataType: "json",
     success: function(response) {
      table.draw();
     },
     error: function(response) {
      console.log("Error: ", response)
     }
    })
    alert(data);
   })

   $(document).on('click', '.editPengajuanBarang', function() {
    $('#myModalLabel').text("Edit Pengajuan Barang");
    $('#tombol-simpan').val("edit-post");
    $('#tambah-edit-modal').modal('show');

    let id = $(this).attr("data-id");
    let pelanggan_id = $(this).attr("data-pelanggan_id");
    let nama_barang = $(this).attr("data-nama_barang");
    let tanggal_pengajuan = $(this).attr("data-tanggal_pengajuan");
    let qty = $(this).attr("data-qty");

    $('.modal-body #Id').val(id);
    $('.modal-body #Pelanggan_id').val(pelanggan_id);
    $('.modal-body #Nama_barang').val(nama_barang);
    $('.modal-body #Tanggal_pengajuan').val(tanggal_pengajuan);
    $('.modal-body #Qty').val(qty);

   });

   $(document).on('click', '.deletePengajuanBarang', function() {
    let id = $(this).attr("data-id");
    iziToast.question({
     timeout: 20000,
     close: false,
     overlay: true,
     displayMode: 'once',
     id: 'question',
     zindex: 999,
     title: 'Yakin?',
     message: 'Data akan terhapus.',
     position: 'center',
     buttons: [
      ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

       $.ajax({
        type: "DELETE",
        url: "{{ route('pengajuan_barang.store') }}/" + id,
        success: function(response) {
         table.draw();
         iziToast.success({
          title: response.title,
          message: response.message,
          position: 'topRight',
         });
        },
        error: function(response) {
         console.log('Error:', response);
         $('#tombol-simpan').html('Simpan');
        }
       });

       instance.hide({
        transitionOut: 'fadeOut'
       }, toast, 'button');

      }, true],
      ['<button>Batal</button>', function(instance, toast) {

       instance.hide({
        transitionOut: 'fadeOut'
       }, toast, 'button');

      }],
     ],
    });
   });

  });
 });
</script>

@endsection