<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <input type="hidden" name="id" id="Id" value="">
          <div class="form-group">
            <label for="pelanggan_id">Nama Pengaju <span class="text-danger">*</span></label>
            <select name="pelanggan_id" class="form-control" id="Pelanggan_id" required tabindex="1">
              <option value="">Pilih..</option>
              @foreach(App\Models\Pelanggan::orderBy('kode_pelanggan')->get() as $pl)
              <option value="{{ $pl->id }}">{{ $pl->kode_pelanggan }} - {{ $pl->nama }}</option>
              @endforeach
            </select>
            <div class="invalid-feedback" id="pelanggan_id_error"></div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="nama_barang">Nama Barang</label>
              <input type="text" class="form-control" name="nama_barang" id="Nama_barang" required tabindex="2">
              <div class="invalid-feedback" id="nama_barang_error"></div>
            </div>
            <div class="form-group col-md-6">
              <label for="nama_barang">Qty</label>
              <input type="number" class="form-control" name="qty" id="Qty" required tabindex="3">
              <div class="invalid-feedback" id="qty_error"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="Tanggal_pengajuan">Tanggal Pengajuan <span class="text-danger">*</span></label>
            <input type="date" class="form-control" name="tanggal_pengajuan" value="{{ date('d/M/Y') }}" tabindex="4" id="Tanggal_pengajuan" autocomplete="off" required>
            <div class="invalid-feedback" id="tanggal_pengajuan_error"></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="5">Simpan</button>
        <button type="submit" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>