@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Data Produk</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item">Data Master</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th width="5%">No</th>
                  <th>Nama Produk</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              <tbody class="text-dark"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@include('produk/modal')
<script>
  $(document).ready(function() {
    $('#Produk_sidebar').parent().addClass('active');
    $('#Produk_sidebar').parent().parent().parent().addClass('active');
    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('produk.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum',
            searchable: false
          },
          {
            data: 'nama_produk',
            name: 'nama_produk'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah Produk");
        $('#tambah-edit-modal').modal('show');
      });

      $("#form-tambah-edit").on('submit', function(e) {
        e.preventDefault();
        $('#tombol-simpan').html('Mengirim..');

        $.ajax({
          data: $('#form-tambah-edit').serialize(),
          url: "{{ route('produk.store') }}",
          type: "POST",
          dataType: 'json',
          success: function(response) {
            $('#tombol-simpan').html('Simpan');
            if (response.icon == 'error') {
              $('#tambah-edit-modal').modal('show');
              iziToast.error({
                title: response.title,
                message: response.message,
                position: 'topRight',
              });
            } else {
              $('#form-tambah-edit').trigger("reset");
              $('#tambah-edit-modal').modal('hide');
              table.draw();
              iziToast.success({
                title: response.title,
                message: response.message,
                position: 'topRight',
              });
            }

          },
          error: function(response) {
            console.log('Error:', response);
            $('#tombol-simpan').html('Simpan');
          }
        });
      })

      $(document).on('click', '.editProduk', function() {
        $('#myModalLabel').text("Edit Produk");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let nama_produk = $(this).attr("data-nama_produk");

        $('.modal-body #Id').val(id);
        $('.modal-body #Nama_Produk').val(nama_produk);

      });

      $(document).on('click', '.deleteProduk', function() {
        let id = $(this).attr("data-id");
        let nama_produk = $(this).attr("data-nama_produk");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data "' + nama_produk + '" akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('produk.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                  $('#tombol-simpan').html('Simpan');
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });
      });

    });
  });
</script>

@endsection