<div class="modal fade" id="tambah-edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" class="needs-validation" novalidate="" id="form-tambah-edit" name="form-tambah-edit">
          <input type="hidden" name="id" id="Id" value="">
          <div class="form-group">
            <label for="nama_produk">Nama Produk <span class="text-danger">*</span></label>
            <input id="Nama_Produk" type="text" class="form-control" name="nama_produk" tabindex="1" maxlength="20" required autofocus>
            <div class="invalid-feedback">Nama Produk tidak boleh kosong</div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary btn-block" id="tombol-simpan" tabindex="2">Simpan</button>
        <button type="submit" class="btn btn-light" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>