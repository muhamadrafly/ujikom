@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-button">
    <a href="javascript:void(0)" id="tombol-tambah" class="btn btn-primary"><i class="fas fa-plus-circle mr-1"></i>Data Pemasok</a>
  </div>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">Menu</div>
    <div class="breadcrumb-item">Data Master</div>
    <div class="breadcrumb-item"><a href="javascript:void(0)">{{ $title }}</a></div>
  </div>
</div>

<div class="section-body">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover" id="data-table" style="width:100%">
              <thead class="text-dark bg-light">
                <tr>
                  <th>No</th>
                  <th>Kode Pemasok</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Kota</th>
                  <th>Telp</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="text-dark"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/cleave.min.js') }}"></script>
<script src="{{ asset('js/cleave-phone.id.js') }}"></script>
@include('pemasok/modal')
<script>
  $(document).ready(function() {
    $('#Pemasok_sidebar').parent().addClass('active');
    $('#Pemasok_sidebar').parent().parent().parent().addClass('active');
    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      let table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: "{{ route('pemasok.index') }}",
        columns: [{
            data: 'rownum',
            name: 'rownum',
            searchable: false
          },
          {
            data: 'kode_pemasok',
            name: 'kode_pemasok'
          },
          {
            data: 'nama_pemasok',
            name: 'nama_pemasok'
          },
          {
            data: 'alamat',
            name: 'alamat'
          },
          {
            data: 'kota',
            name: 'kota'
          },
          {
            data: 'no_telp',
            name: 'no_telp'
          },
          {
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
          },
        ],
      });

      $('#tombol-tambah').click(function() {
        $('#button-simpan').val("create-post");
        $('#Id').val('');
        $('#form-tambah-edit').trigger("reset");
        $('#myModalLabel').text("Tambah Pemasok");
        $('#tambah-edit-modal').modal('show');
      });

      if ($("#form-tambah-edit").length > 0) {
        $("#form-tambah-edit").validate({
          messages: {
            nama_pemasok: "Nama Pemasok tidak boleh kosong",
            alamat: "Alamat tidak boleh kosong",
            kota: "Kota tidak boleh kosong",
            no_telp: {
              required: "Telp tidak boleh kosong",
              minlength: "Telp minimal 8 digit",
            },
          },
          errorPlacement: function(error, element) {
            let name = element.attr("name");
            $("#" + name + "_error").html(error);
          },

          submitHandler: function(form) {
            $('#tombol-simpan').html('Mengirim..');

            $.ajax({
              data: $('#form-tambah-edit').serialize(),
              url: "{{ route('pemasok.store') }}",
              type: "POST",
              dataType: 'json',
              success: function(response) {
                $('#tombol-simpan').html('Simpan');
                if (response.icon == 'error') {
                  $('#tambah-edit-modal').modal('show');
                  iziToast.error({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                } else {
                  $('#form-tambah-edit').trigger("reset");
                  $('#tambah-edit-modal').modal('hide');
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                }
              },
              error: function(response) {
                console.log('Error:', response);
                $('#tombol-simpan').html('Simpan');
              }
            });
          }
        });
      }

      $(document).on('click', '.editPemasok', function() {
        $('#myModalLabel').text("Edit Pemasok");
        $('#tombol-simpan').val("edit-post");
        $('#tambah-edit-modal').modal('show');

        let id = $(this).attr("data-id");
        let kode_pemasok = $(this).attr("data-kode_pemasok");
        let nama_pemasok = $(this).attr("data-nama_pemasok");
        let alamat = $(this).attr("data-alamat");
        let kota = $(this).attr("data-kota");
        let no_telp = $(this).attr("data-no_telp");

        $('.modal-body #Id').val(id);
        $('.modal-body #Kode_pemasok').val(kode_pemasok);
        $('.modal-body #nama_pemasok').val(nama_pemasok);
        $('.modal-body #Alamat').val(alamat);
        $('.modal-body #Kota').val(kota);
        $('.modal-body #No_telp').val(no_telp);

      });

      $(document).on('click', '.deletePemasok', function() {
        let id = $(this).attr("data-id");
        let nama_pemasok = $(this).attr("data-nama_pemasok");
        iziToast.question({
          timeout: 20000,
          close: false,
          overlay: true,
          displayMode: 'once',
          id: 'question',
          zindex: 999,
          title: 'Yakin?',
          message: 'Data "' + nama_pemasok + '" akan terhapus.',
          position: 'center',
          buttons: [
            ['<button><b>Ya, </b>Hapus</button>', function(instance, toast) {

              $.ajax({
                type: "DELETE",
                url: "{{ route('pemasok.store') }}/" + id,
                success: function(response) {
                  table.draw();
                  iziToast.success({
                    title: response.title,
                    message: response.message,
                    position: 'topRight',
                  });
                },
                error: function(response) {
                  console.log('Error:', response);
                  $('#tombol-simpan').html('Simpan');
                }
              });

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }, true],
            ['<button>Batal</button>', function(instance, toast) {

              instance.hide({
                transitionOut: 'fadeOut'
              }, toast, 'button');

            }],
          ],
        });
      });

    });
  });
</script>

@endsection