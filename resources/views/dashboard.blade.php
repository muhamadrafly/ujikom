@extends('templates/master')
@section('content')

<div class="section-header">
  <h1>{{ $title }}</h1>
  <div class="section-header-breadcrumb">
    <div class="breadcrumb-item active">

    </div>
  </div>
</div>
<div class="section-body">
  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-primary">
          <i class="fas fa-hand-holding-usd"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pendapatan</h4>
          </div>
          <div class="card-body">
            <small class="font-weight-bold">@php
              $total_pendapatan = 0;
              foreach(App\Models\Penjualan::get() as $tp) {
              $total_pendapatan += $tp->total_bayar;
              }
              @endphp
              Rp. {{ number_format($total_pendapatan,0) }}</small>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-danger">
          <i class="fas fa-money-check"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pembelian</h4>
          </div>
          <div class="card-body">
            <small class="font-weight-bold">@php
              $total_pembelian = 0;
              foreach(App\Models\Pembelian::get() as $tp) {
              $total_pembelian += $tp->total;
              }
              @endphp
              Rp. {{ number_format($total_pembelian,0) }}</small>

          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-warning">
          <i class="fas fa-boxes"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Barang</h4>
          </div>
          <div class="card-body">
            <small class="font-weight-bold">@php
              $jml_barang = count(App\Models\Barang::get());
              @endphp
              {{ $jml_barang }}</small>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="col-lg-4 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-success">
          <i class="far fa-user"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Pelanggan</h4>
          </div>
          <div class="card-body">
            <small class="font-weight-bold">@php
              $jml_pelanggan = count(App\Models\Pelanggan::get());
              @endphp
              {{ $jml_pelanggan }}</small>
          </div>
        </div>
      </div>
    </div> -->


  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
      <div class="card">
        <div class="card-header">
          <h4>Data Penjualan 30 Hari Terakhir</h4>
          <div class="card-header-action">
          </div>
        </div>
        <div class="card-body">
          <canvas id="myChart" height="100"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/chart.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#Dashboard_sidebar').parent().addClass('active');

    let ctx = document.getElementById('myChart');
    let myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [

        ],
        datasets: [{
            label: 'Pendapatan Perhari',
            data: [

            ],
            backgroundColor: 'RGB(44, 77, 240)',
            borderColor: 'RGB(44, 77, 240)',
            borderWidth: 2,
          },
          {
            label: 'Jumlah Transaksi',
            data: [

            ],
            backgroundColor: 'rgb(75, 192, 192)',
            borderColor: 'rgb(75, 192, 192)',
            borderWidth: 2,
          },
        ]
      },
      options: {
        plugins: {
          title: {
            display: false,

          },
          legend: {
            display: true,
          }
        },
        scales: {
          x: {
            beginAtZero: false
          },
          y: [{
            ticks: {
              callback: function(value, index, values) {
                if (parseInt(value) >= 1000) {
                  return 'Rp. ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } else {
                  return 'Rp. ' + value;
                }
              }
            }
          }],
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        }
      }
    });

    let UpdateData = function() {
      $.ajax({
        url: "{{ route('home.create') }}",
        type: "GET",
        dataType: "Json",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
          myChart.data = {
            labels: response.labels,
            datasets: [{
                label: 'Pendapatan Perhari',
                data: response.pendapatan,
                backgroundColor: 'RGB(44, 77, 240)',
                borderColor: 'RGB(44, 77, 240)',
                borderWidth: 2,
              },
              {
                label: 'Jumlah Transaksi',
                data: response.jml_data,
                backgroundColor: 'rgb(75, 192, 192)',
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 2,
              },
            ]
          }
          myChart.update();
        },
        error: function(response) {
          console.log(response.pendapatan);
        }
      })
    }

    UpdateData();
    setInterval(() => {
      UpdateData();
    }, 2500);
  });
</script>


@endsection