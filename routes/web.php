<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
  return view('login');
});

Route::resource('auth', 'App\Http\Controllers\AuthController');
Route::middleware(['auth', 'isAdmin:1,0'])->group(function () {

  Route::prefix('menu/')->group(function () {
    Route::resource('home', 'App\Http\Controllers\DashboardController');
    Route::resource('profile', 'App\Http\Controllers\ProfileController');
    Route::resource('penarikan_barang', 'App\Http\Controllers\PenarikanBarangController');
    Route::resource('pengajuan_barang', 'App\Http\Controllers\PengajuanBarangController');

    Route::prefix('master_data/')->group(function () {
      Route::resource('produk', 'App\Http\Controllers\ProdukController');
      Route::resource('barang', 'App\Http\Controllers\BarangController');
      Route::resource('pemasok', 'App\Http\Controllers\PemasokController');
      Route::resource('pelanggan', 'App\Http\Controllers\PelangganController');
    });

    Route::prefix('transaksi/')->group(function () {
      Route::resource('pembelian', 'App\Http\Controllers\PembelianController');
      Route::resource('penjualan', 'App\Http\Controllers\PenjualanController');
    });

    Route::prefix('laporan/')->group(function () {
      Route::resource('laporan_pembelian', 'App\Http\Controllers\LaporanPembelianController');
      Route::resource('laporan_penjualan', 'App\Http\Controllers\LaporanPenjualanController');
    });

    Route::get('logout', function () {
      Auth::logout();
      return redirect('auth');
    });
  });

  Route::prefix('admin/user_manage/')->group(function () {
    Route::middleware(['isAdmin:1'])->group(function () {
      Route::resource('account', 'App\Http\Controllers\UserController');
      Route::resource('access', 'App\Http\Controllers\AccessController');
    });
  });
});
