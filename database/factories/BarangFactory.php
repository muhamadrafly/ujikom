<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Barang;
use App\Models\Produk;

class BarangFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Barang::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'id'          => $this->faker->Uuid,
      'kode_barang' => sprintf('B%08d', $this->faker->unique()->numberBetween(0, 99999999)),
      'produk_id'   => $this->faker->randomElement(Produk::select('id')->get()),
      'nama_barang' => $this->faker->randomElement([
        'Indomie Goreng', 'Indomie Ayam Bawang', 'Pepsodent Charcoal', 'Sarimi isi(2) Soto', 'Extra jos Ginseng', 'Sedap Soto Lamongan', 'Le Mineral'
      ]),
      'satuan'      => $this->faker->randomElement(['pcs', 'item', 'kardus']),
      'harga_jual'  => $this->faker->numberBetween(1000, 20000),
      'stok'        => $this->faker->numberBetween(1, 250),
    ];
  }
}
