<?php

namespace Database\Factories;

use App\Models\Barang;
use App\Models\DetailPenjualan;
use App\Models\Penjualan;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailPenjualanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailPenjualan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $id_penjualan = $this->faker->randomElement(Penjualan::select('id')->whereIn('tgl_faktur', ['2021-05-07'])->get());
        $barang_id  = $this->faker->randomElement(Barang::select('id')->get());
        $harga = Barang::select('harga_jual')->where('id', $barang_id->id)->first();
        $jumlah = $this->faker->numberBetween(1, 20);
        return [
            'id' => $this->faker->uuid,
            'penjualan_id' => $id_penjualan,
            'barang_id' => $barang_id,
            'harga_jual' => $harga['harga_jual'],
            'jumlah' => $jumlah,
            'sub_total' => $harga->harga_jual * $jumlah,
        ];
    }
}
