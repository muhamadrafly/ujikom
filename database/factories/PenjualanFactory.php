<?php

namespace Database\Factories;

use App\Models\Pelanggan;
use App\Models\Penjualan;
use App\Models\User;
use DateTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class PenjualanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Penjualan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $date = new DateTime();
        $date->setDate(2021, 05, 07);
        return [
            'id'            => $this->faker->Uuid,
            'no_faktur'     => sprintf('T%08d', $this->faker->unique()->numberBetween(0, 99999999)),
            'tgl_faktur'    => $date->format('Y-m-d'),
            'total_bayar'   => $this->faker->numberBetween(1000, 1000000),
            'pelanggan_id'  => $this->faker->randomElement(Pelanggan::select('id')->get()),
            'user_id'       => $this->faker->randomElement(User::select('id')->get()),
        ];
    }
}
