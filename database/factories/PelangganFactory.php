<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Pelanggan;

class PelangganFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Pelanggan::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'id'             => $this->faker->Uuid,
      'kode_pelanggan' => sprintf('C%08d', $this->faker->unique()->numberBetween(0, 99999999)),
      'nama'           => $this->faker->name,
      'alamat'         => $this->faker->address,
      'no_telp'        => $this->faker->phoneNumber,
      'email'          => $this->faker->unique()->safeEmail,
    ];
  }
}
