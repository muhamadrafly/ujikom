<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Produk;

class ProdukFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Produk::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'id'          => $this->faker->uuid,
      'nama_produk' => $this->faker->randomElement([
        'Mie Instan', 'Pasta Gigi'
      ]),
    ];
  }
}
