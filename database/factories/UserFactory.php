<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'                => $this->faker->Uuid,
            'nama'              => $this->faker->name,
            'is_admin'          => $this->faker->numberBetween(0, 1),
            'email'             => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'username'          => $this->faker->unique()->username,
            'password'          => Hash::make($this->faker->randomElement(['admin'])),
            'remember_token'    => Str::uuid(),
        ];
    }
}
