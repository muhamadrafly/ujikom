<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Pemasok;

class PemasokFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pemasok::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'           => $this->faker->Uuid,
            'kode_pemasok' => sprintf('S%08d', $this->faker->unique()->numberBetween(0, 99999999)),
            'nama_pemasok' => $this->faker->company,
            'alamat'       => $this->faker->address,
            'kota'         => $this->faker->city,
            'no_telp'      => $this->faker->phoneNumber,
        ];
    }
}
