<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Acces;
use App\Models\User;


class AccesFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Acces::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */

  public function definition()
  {
    return [
      'id'               => $this->faker->Uuid,
      'user_id'          => $this->faker->randomElement(User::select('id')->get()),
      'kelola_barang'    => 1,
      'kelola_pasok'     => 1,
      'kelola_penjualan' => 1,
      'kelola_laporan'   => 1,
    ];
  }
}
