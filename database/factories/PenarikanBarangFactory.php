<?php

namespace Database\Factories;

use App\Models\Barang;
use App\Models\PenarikanBarang;
use Illuminate\Database\Eloquent\Factories\Factory;

class PenarikanBarangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PenarikanBarang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'                => $this->faker->Uuid,
            'barang_id'         => $this->faker->randomElement(Barang::select('id')->get()),
            'tanggal_expired'   => $this->faker->dateTime(),
            'ditarik'           => $this->faker->numberBetween(0,1)
        ];
    }
}
