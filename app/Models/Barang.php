<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Barang extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'kode_barang', 'produk_id', 'nama_barang', 'satuan', 'harga_jual', 'stok', 'user_id'
    ];

    public function Produks()
    {
        return $this->belongsTo('App\Models\Produk', 'produk_id', 'id');
    }
}
