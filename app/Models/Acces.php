<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Acces extends Model
{
    use HasFactory;

    protected $table = 'access';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'user_id', 'kelola_pasok', 'kelola_penjualan', 'kelola_barang', 'kelola_laporan'
    ];

    public function Users()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
