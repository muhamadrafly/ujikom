<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PengajuanBarang extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_barangs';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'pelanggan_id', 'tanggal_pengajuan', 'terpenuhi','qty','nama_barang' 
    ];

    public function Pelanggan()
    {
        return $this->belongsTo('App\Models\Pelanggan', 'pelanggan_id', 'id');
    }
}
