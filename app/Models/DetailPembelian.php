<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DetailPembelian extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'pembelian_id', 'barang_id', 'harga_beli', 'jumlah', 'sub_total'
    ];

    public function Pembelians()
    {
        return $this->belongsTo('App\Models\Pembelian', 'pembelian_id', 'id');
    }

    public function Barangs()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id', 'id');
    }
}
