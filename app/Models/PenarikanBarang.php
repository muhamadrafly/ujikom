<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PenarikanBarang extends Model
{
    use HasFactory;

    protected $table = 'penarikan_barangs';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    protected $fillable = [
        'barang_id', 'tanggal_expired', 'ditarik',
    ];

    public function Barangs()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id', 'id');
    }
}
