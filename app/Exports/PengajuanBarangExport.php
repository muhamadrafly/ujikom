<?php

namespace App\Exports;

use App\Models\PengajuanBarang;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PengajuanBarangExport implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping
{
    public function query()
    {
        $pengajuan_barang = PengajuanBarang::query();
        return $pengajuan_barang;
    }
    public function map($pengajuan_barang): array
    {
        if ($pengajuan_barang->terpenuhi == 0) {
            $terpenuhi = "TIDAK TERPENUHI";
        } else {
            $terpenuhi = "TERPENUHI";
        }
        return [
            $pengajuan_barang->Pelanggan->nama,
            $pengajuan_barang->nama_barang,
            $pengajuan_barang->tanggal_pengajuan,
            $pengajuan_barang->qty,
            $terpenuhi,
            date('d M, Y', strtotime($pengajuan_barang->created_at)),
            date('d M, Y', strtotime($pengajuan_barang->updated_at)),
        ];
    }
    public function headings(): array
    {
        return [
            'Nama Pengaju',
            'Nama Barang',
            'Tanggal Pengajuan',
            'QTY',
            'Status',
            'Tanggal Dibuat',
            'Tanggal Terakhir DiEdit'
        ];
    }
}
