<?php

namespace App\Exports;

use App\Models\PenarikanBarang;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PenarikanBarangExport implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping
{
    public function query()
    {
        $penarikan_barang = PenarikanBarang::query();
        return $penarikan_barang;
    }
    public function map($penarikan_barang): array
    {
        if ($penarikan_barang->ditarik == 0) {
            $ditarik = "TIDAK DITARIK";
        } else {
            $ditarik = "DITARIK";
        }
        return [
            $penarikan_barang->Barangs->kode_barang,
            $penarikan_barang->Barangs->nama_barang,
            $penarikan_barang->tanggal_expired,
            $penarikan_barang->Barangs->stok,
            $ditarik,
            date('d M, Y', strtotime($penarikan_barang->created_at)),
            date('d M, Y', strtotime($penarikan_barang->updated_at)),
        ];
    }
    public function headings(): array
    {
        return [
            'Kode Barang',
            'Nama Barang',
            'Tanggal Expired',
            'Stok',
            'Status',
            'Created at',
            'Updated at'
        ];
    }
}
