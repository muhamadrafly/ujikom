<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle(Request $request, Closure $next, ...$roles)
  {
    if (Auth::check()) {
      if (in_array(Auth::user()->is_admin, $roles)) {
        return $next($request);
      } else {
        return redirect()->route('home.create');
      }
    } else {
      return redirect()->route('home.create');
    }
  }
}
