<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Acces;
use Yajra\DataTables\DataTables;

class AccessController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if (Auth::user()->is_admin == 1 && Auth::check() == true) {

      if ($request->ajax()) {
        $access = Acces::get();
        return DataTables::of($access)
          ->editColumn('user_id', function ($row) {
            if ($row->Users->is_admin == 1) {
              $user_details = '<span class="d-block mb-1 text-dark">' . $row->Users->nama . ' - <span class="text-danger">Administrator</span></span>';
            } else {
              $user_details = '<span class="d-block mb-1 text-dark">' . $row->Users->nama . ' - <span class="text-warning">Cashier</span></span>';
            }
            $user_details = $user_details . '<span>' . $row->Users->email . '</span>';
            return $user_details;
          })
          ->editColumn('kelola_barang', function ($row) {
            if ($row->kelola_barang == 1) {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_barang" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_barang' . $row->id . '" checked>';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_barang' . $row->id . '">Checked</label></div>';
            } else {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_barang" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_barang' . $row->id . '">';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_barang' . $row->id . '">Unchecked</label></div>';
            }
            return $checkBox;
          })
          ->editColumn('kelola_pasok', function ($row) {
            if ($row->kelola_pasok == 1) {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_pasok" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_pasok' . $row->id . '" checked>';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_pasok' . $row->id . '">Checked</label></div>';
            } else {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_pasok" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_pasok' . $row->id . '">';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_pasok' . $row->id . '">Unchecked</label></div>';
            }
            return $checkBox;
          })
          ->editColumn('kelola_penjualan', function ($row) {
            if ($row->kelola_penjualan == 1) {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_penjualan" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_penjualan' . $row->id . '" checked>';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_penjualan' . $row->id . '">Checked</label></div>';
            } else {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_penjualan" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_penjualan' . $row->id . '">';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_penjualan' . $row->id . '">Unchecked</label></div>';
            }
            return $checkBox;
          })
          ->editColumn('kelola_laporan', function ($row) {
            if ($row->kelola_laporan == 1) {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_laporan" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_laporan' . $row->id . '" checked>';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_laporan' . $row->id . '">Checked</label></div>';
            } else {
              $checkBox = '<div class="custom-control custom-checkbox">';
              $checkBox = $checkBox . '<input data-access="kelola_laporan" data-user_id=' . $row->user_id . ' data-is_admin=' . $row->Users->is_admin . ' type="checkbox" class="custom-control-input" id="kelola_laporan' . $row->id . '">';
              $checkBox = $checkBox . '<label class="custom-control-label" for="kelola_laporan' . $row->id . '">Unchecked</label></div>';
            }
            return $checkBox;
          })
          ->rawColumns(['user_id', 'kelola_barang', 'kelola_pasok', 'kelola_penjualan', 'kelola_laporan'])
          ->make(true);
      }

      return view('account.access', ['title' => "Access"]);
    } else {
      return back();
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if (Auth::user()->is_admin == 1 && Auth::check() == true) {
      $namaAccess = $request->access;
      $acces = Acces::where('user_id', $request->id)->first();
      if ($acces->$namaAccess == 1) {
        Acces::where('user_id', $request->id)->update([$namaAccess => 0]);
      } else {
        Acces::where('user_id', $request->id)->update([$namaAccess => 1]);
      }
      return response()->json(['success' => 'Data canged successfully']);
    } else {
      return back();
    }
  }
}
