<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\str;

use App\Models\Pelanggan;
use App\Models\Acces;
use Yajra\DataTables\DataTables;

class PelangganController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_penjualan == 1 && Auth::check() == true) {
      $lastId = Pelanggan::select('kode_pelanggan')->orderBy('kode_pelanggan', 'desc')->first();
      $kode = ($lastId == null ? 'M0000001' : sprintf('M%07d', substr($lastId->kode_pelanggan, 1) + 1));

      if ($request->ajax()) {
        $pelanggan = DB::table(DB::raw('pelanggans, (SELECT @rownum := 0) r'))
          ->select('pelanggans.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->where('kode_pelanggan', '!=', 'M0000000')->get();
        return DataTables::of($pelanggan)
          ->addColumn('action', function ($row) {
            $btn = '<div class="dropdown d-inline">';
            $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
            $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
            $btn = $btn . '<a class="dropdown-item has-icon editPelanggan" href="javascript:void(0)"  data-id="' . $row->id . '" data-kode_pelanggan="' . $row->kode_pelanggan . '"data-nama="' . $row->nama . '" data-alamat="' . $row->alamat . '" data-no_telp="' . $row->no_telp . '" data-email="' . $row->email . '"><i class="far fa-edit"></i> Edit</a>';
            $btn = $btn . '<a class="dropdown-item has-icon deletePelanggan" href="javascript:void(0)" data-id="' . $row->id . '" data-nama="' . $row->nama . '" ><i class="fas fa-trash"></i> Hapus</a>';
            $btn = $btn . '</div>';
            $btn = $btn . '</div>';
            return $btn;
          })
          ->rawColumns(['action'])
          ->make(true);
      }
      return view('pelanggan.index')->with(['title' => "Pelanggan", 'kode' => $kode]);
    } else {
      return back();
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_penjualan == 1 && Auth::check() == true) {
      if (empty($request->id)) {
        $check_kode_pelanggan = Pelanggan::where('kode_pelanggan', $request->kode_pelanggan)->count();
        $check_email_pelanggan = Pelanggan::where('email', $request->email)->count();
        if ($check_kode_pelanggan != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Kode Pelanggan sudah tersedia harap refresh terlebih dahulu', 401]);
        } elseif ($check_email_pelanggan != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Email Sudah Digunakan', 401]);
        } else {
          $pelanggan = new Pelanggan;
          $pelanggan->id = Str::uuid();
          $pelanggan->kode_pelanggan = $request->kode_pelanggan;
          $pelanggan->nama = $request->nama;
          $pelanggan->alamat = $request->alamat;
          $pelanggan->no_telp = $request->no_telp;
          $pelanggan->email = $request->email;
          $pelanggan->save();
          return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
        }
      } else {
        $check_id_pelanggan = Pelanggan::find($request->id);
        $check_email_pelanggan = Pelanggan::where('email', $request->email)->count();
        if ($check_email_pelanggan != 0 && $check_id_pelanggan->email != $request->email) {
          return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Email Sudah Digunakan', 401]);
        } else {
          $pelanggan = Pelanggan::where('id', $request->id)->update($request->all());
          return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
        }
      }
    } else {
      return back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_penjualan == 1 && Auth::check() == true) {
      Pelanggan::find($id)->delete();
      return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    } else {
      return back();
    }
  }
}
