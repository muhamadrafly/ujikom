<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() == true) {
            return view('profile', ['title' => 'Profile']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_account = User::find(Auth::id());
        $check_email = User::all()->where('email', $request->email)->count();
        $check_username = User::all()->where('username', $request->username)->count();

        if (($check_email == 0 && $check_username == 0) || ($user_account->email == $request->email && $user_account->username == $request->username) || ($check_email == 0 && $user_account->username == $request->username) || ($user_account->email == $request->email && $check_username == 0)) {
            $user_account->nama = $request->nama;
            $user_account->email = $request->email;
            $user_account->username = $request->username;
            $user_account->save();
            return response()->json(['title' => 'Success', 'icon' => 'success', 'message' => 'Profile berhasil diubah', 200]);
        } else if ($check_email != 0 && $check_username != 0 && $user_account->email != $request->email && $user_account->username != $request->username) {
            return response()->json(['title' => 'Gagal', 'icon' => 'error', 'message' => 'Email dan username sudah digunakan', 401]);
        } else if ($check_email != 0 && $user_account->email != $request->email) {
            return response()->json(['title' => 'Gagal', 'icon' => 'error', 'message' => 'Email sudah digunakan', 401]);
        } else if ($check_username != 0 && $user_account->username != $request->username) {
            return response()->json(['title' => 'Gagal', 'icon' => 'error', 'message' => 'Username sudah digunakan', 401]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);
        if (Hash::check($request->password_lama, $users->password)) {
            User::where('id', Auth::id())->update(['password' => Hash::make($request->password_baru)]);
            return response()->json(['title' => 'Success', 'icon' => 'success', 'message' => 'Password berhasil diubah', 200]);
        } else {
            return response()->json(['title' => 'Gagal', 'icon' => 'error', 'message' => 'Password lama tidak sesuai', 401]);
        }
    }
}
