<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use App\Models\Pelanggan;
use App\Models\PengajuanBarang;
use Yajra\DataTables\DataTables;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PengajuanBarangExport;
use App\Models\Logging;
use PDF;

class PengajuanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data_pengajuan = DB::table(DB::raw('pengajuan_barangs, (SELECT @rownum := 0) r'))
        ->select('pengajuan_barangs.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->get();
        if ($request->ajax()) {
            return DataTables::of($data_pengajuan)
                ->addColumn('action', function ($row) {
                    $btn = '<div class="dropdown d-inline">';
                    $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
                    $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
                    $btn = $btn . '<a class="dropdown-item has-icon editPengajuanBarang" href="javascript:void(0)"  data-id="' . $row->id . '" data-pelanggan_id="' . $row->pelanggan_id . '" data-nama_barang="' . $row->nama_barang . '" data-tanggal_pengajuan="' . $row->tanggal_pengajuan . '" data-qty="' . $row->qty . '"><i class="far fa-edit"></i> Edit</a>';
                    $btn = $btn . '<a class="dropdown-item has-icon deletePengajuanBarang" href="javascript:void(0)" data-id="' . $row->id . '"><i class="fas fa-trash"></i> Hapus</a>';
                    $btn = $btn . '</div>';
                    $btn = $btn . '</div>';
                    return $btn;
                })
                ->editColumn('pelanggan_id', function ($row) {
                    $pelanggan = Pelanggan::where('id', $row->pelanggan_id)->first();
                    return $pelanggan->nama;
                })
                ->editColumn('terpenuhi', function ($row) {
                    if ($row->terpenuhi == 1 && Auth::user()->is_admin == 1) {
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" name="terpenuhi_status" checked class="custom-switch-input" data-id = "'. $row->id . '" data-terpenuhi = "' . $row->terpenuhi . '">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    } elseif($row->terpenuhi == 0 && Auth::user()->is_admin == 1){
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" name="terpenuhi_status" class="custom-switch-input" data-id = "' . $row->id . '" data-terpenuhi = "' . $row->terpenuhi . '">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    } else if ($row->terpenuhi == 1 && Auth::user()->is_admin == 0) {
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" disabled="" checked class="custom-switch-input">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    } else {
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" disabled="" class="custom-switch-input">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    }
                    return $toggle;
                })
                ->rawColumns(['action', 'terpenuhi'])
                ->make(true);
        }
        return view('pengajuan_barang.index', ['title' => "Pengajuan Barang"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Excel::download(new PengajuanBarangExport, 'penarikan_barang-' . time() . '-' . rand() . '.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $penarikan_barang = new PengajuanBarang();
            $penarikan_barang->id = Str::uuid();
            $penarikan_barang->pelanggan_id = $request->pelanggan_id;
            $penarikan_barang->nama_barang = $request->nama_barang;
            $penarikan_barang->tanggal_pengajuan = $request->tanggal_pengajuan;
            $penarikan_barang->qty = $request->qty;
            $penarikan_barang->save();
            return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
        } else {
            PengajuanBarang::where('id', $request->id)->update($request->all());
            return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PengajuanBarang::get();
        $pdf = PDF::loadView('pengajuan_barang.export_pdf', compact('data'));
        return $pdf->stream('pengajuan_barang-' . time() . '-' . rand() . '.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->terpenuhi == 1) {
            PengajuanBarang::where('id', $request->id)->update(['terpenuhi' => 0]);
        } else {
            PengajuanBarang::where('id', $request->id)->update(['terpenuhi' => 1]);
        }
        return response()->json(['title' => 'Change Success', 'message' => 'Status Berhasil Diubah', 200]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PengajuanBarang::find($id)->delete();
        return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    }
}
