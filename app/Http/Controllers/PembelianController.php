<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Pembelian;
use App\Models\Barang;
use App\Models\Acces;
use App\Models\DetailPembelian;

class PembelianController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_pasok == 1) {
      $lastId = Pembelian::select('kode_masuk')->orderBy('created_at', 'desc')->first();
      $kode = ($lastId == null ? 'P' . date('dmy') . '0001' : sprintf('P%04d', substr($lastId->kode_masuk, 1) + 1));
      return view('pembelian.index')->with(['title' => "Pembelian", 'kode' =>  $kode]);
    } else {
      return back();
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_pasok == 1 && Auth::check() == true) {

      $check_kode_pembelian = Pembelian::where('kode_masuk', $request->kode_pembelian)->count();
      if ($check_kode_pembelian != 0) {
        return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Kode Pembelian sudah tersedia harap refresh terlebih dahulu', 401]);
      } else {
        $pembelian = new Pembelian;
        $pembelian->id = Str::uuid();
        $pembelian->kode_masuk = $request->kode_pembelian;
        $pembelian->tanggal_masuk = $request->tanggal_masuk;
        $pembelian->total = $request->total_harga;
        $pembelian->pemasok_id = $request->pemasok_id;
        $pembelian->user_id = Auth::id();
        $pembelian->save();

        $jml_barang = count($request->barang_id);
        for ($i = 0; $i < $jml_barang; $i++) {
          $detailPembelian = new DetailPembelian;
          $detailPembelian->pembelian_id = $pembelian->id;
          $detailPembelian->barang_id = $request->barang_id[$i];
          $detailPembelian->harga_beli = $request->harga_beli[$i];
          $detailPembelian->jumlah = $request->jumlah[$i];
          $detailPembelian->sub_total = $request->subtotal[$i];
          $detailPembelian->save();
        }
        $lastId = Pembelian::select('kode_masuk')->orderBy('created_at', 'desc')->first();
        $kode = ($lastId == null ? 'P' . date('dmy') . '0001' : sprintf('P%04d', substr($lastId->kode_masuk, 1) + 1));
        return response()->json(['kode' => $kode, 'title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
      }
    } else {
      return back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show($kode)
  {
    // $checkBarang = Barang::where('kode_barang', $kode)->first();
    // return response()->json(['barang' => $checkBarang]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //Data Barang Value
    $barang = Barang::find($id);

    //Data For Statistic Value
    $detailPembelian = DetailPembelian::where('barang_id', $id)
      ->latest()
      ->take(7)
      ->get();
    $tanggal = array();
    $harga_beli = array();
    foreach ($detailPembelian as $no => $detPem) {
      $tanggal[$no] = date('d M, y', strtotime($detPem->created_at));
      $harga_beli[$no] = $detPem->harga_beli;
    }
    $tanggal_rev = array_reverse($tanggal);
    $harga_beli_rev = array_reverse($harga_beli);

    //Data Count Pemasok Value
    $data = DetailPembelian::where('barang_id', $id)->get();
    $array_jml_data = array();
    foreach ($data as $no => $dt) {
      $array_jml_data[$no] =  $dt->Pembelians->pemasok_id;
    }
    $jumlahPemasok = count(array_unique($array_jml_data)) . ' Pemasok';

    return response()->json([
      'barang' => $barang,
      'jumlah_pemasok' => $jumlahPemasok,
      'tanggal' => $tanggal_rev,
      'harga_beli' => $harga_beli_rev
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update($id)
  {
    $tableRefresh = DetailPembelian::where('barang_id', $id)->orderBy('created_at', 'desc')->get();
    return view('pembelian.statistic_table', compact('tableRefresh'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
