<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetailPenjualan;
use App\Models\Pemasok;
use App\Models\Penjualan;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard', ['title' => 'Dashboard']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Penjualan = Penjualan::orderBy('tgl_faktur', 'desc')->get();
        $array = array();
        foreach ($Penjualan as $key => $value) {
            array_push($array, $value->tgl_faktur);
        }
        $dates = array_unique($array);
        rsort($dates);

        $jml_dates = count($dates);
        $labless = array();
        if ($jml_dates > 30) {
            for ($i = 0; $i < 30; $i++) {
                array_push($labless, $dates[$i]);
            }
        } elseif ($jml_dates > 0) {
            for ($i = 0; $i < $jml_dates; $i++) {
                array_push($labless, $dates[$i]);
            }
        }

        $labels = array_reverse($labless);
        $pendapatan = array();
        $jml_data = array();
        foreach ($labels as $value) {
            array_push($pendapatan, Penjualan::whereDate('tgl_faktur', $value)->sum('total_bayar') / 100000);
            array_push($jml_data, count(Penjualan::whereDate('tgl_faktur', $value)->get()));
        }
        return response()->json(compact('labels', 'jml_data', 'pendapatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function error()
    {
    }
}
