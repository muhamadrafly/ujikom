<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Barang;
use App\Models\Acces;
use Yajra\DataTables\DataTables;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $checkAccess = Acces::where('user_id', Auth::id())->first();
        if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {
            $lastId = Barang::select('kode_barang')->orderBy('kode_barang', 'desc')->first();
            $kode = ($lastId == null ? 'B0000001' : sprintf('B%07d', substr($lastId->kode_barang, 1) + 1));

            if ($request->ajax()) {
                DB::statement(DB::raw('set @rownum := 0'));
                $barang = Barang::select(DB::raw('@rownum := 0 r'))
                    ->select(DB::raw('@rownum := @rownum + 1 AS rownum'), 'barangs.*');
                return DataTables::of($barang)
                    ->addColumn('action', function ($row) {
                        $btn = '<div class="dropdown d-inline">';
                        $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
                        $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
                        $btn = $btn . '<a class="dropdown-item has-icon editBarang" href="javascript:void(0)"  data-id="' . $row->id . '" data-kode_barang="' . $row->kode_barang . '" data-produk_id="' . $row->produk_id . '" data-nama_barang="' . $row->nama_barang . '" data-satuan="' . $row->satuan . '" data-harga_jual="' . $row->harga_jual . '" data-stok="' . $row->stok . '"><i class="far fa-edit"></i> Edit</a>';
                        $btn = $btn . '<a class="dropdown-item has-icon deleteBarang" href="javascript:void(0)" data-id="' . $row->id . '" data-nama_barang="' . $row->nama_barang . '"><i class="fas fa-trash"></i> Hapus</a>';
                        $btn = $btn . '</div>';
                        $btn = $btn . '</div>';
                        return $btn;
                    })
                    ->editColumn('produk_id', function ($row) {
                        return $row->Produks->nama_produk;
                    })
                    ->editColumn('harga_jual', function ($row) {
                        return 'Rp. ' . number_format($row->harga_jual, 0);
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }

            return view('barang.index', ['title' => "Barang", 'kode' => $kode]);
        } else {
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkAccess = Acces::where('user_id', Auth::id())->first();
        if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {
            if (empty($request->id)) {
                $check_kode_barang = Barang::where('kode_barang', $request->kode_barang)->count();
                if ($check_kode_barang != 0) {
                    return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Kode Barang sudah tersedia harap refresh terlebih dahulu', 401]);
                } else {
                    $barang = new barang;
                    $barang->id = Str::uuid();
                    $barang->kode_barang = $request->kode_barang;
                    $barang->produk_id = $request->produk_id;
                    $barang->nama_barang = $request->nama_barang;
                    $barang->satuan = $request->satuan;
                    $barang->harga_jual = $request->harga_jual;
                    $barang->stok = $request->stok;
                    $barang->save();
                    return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
                }
            } else {
                $barang = Barang::where('id', $request->id)->update($request->all());
                return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
            }
        } else {
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkAccess = Acces::where('user_id', Auth::id())->first();
        if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {
            Barang::find($id)->delete();
            return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
        } else {
            return back();
        }
    }
}
