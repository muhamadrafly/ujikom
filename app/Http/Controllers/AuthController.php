<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() == true) {
            return back();
        } else {
            return view('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rememberMe = false;
        if (!empty($request->remember)) {
            $rememberMe = true;
        }
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password], $rememberMe)) {
            return redirect()->route('penjualan.index');
        }
        return back()->with('fail', 'username atau password salah');
    }
}
