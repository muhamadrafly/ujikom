<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Pemasok;
use App\Models\Acces;
use Yajra\DataTables\DataTables;

class PemasokController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_pasok == 1 && Auth::check() == true) {
      $lastId = Pemasok::select('kode_pemasok')->orderBy('kode_pemasok', 'desc')->first();
      $kode = ($lastId == null ? 'S0000001' : sprintf('S%07d', substr($lastId->kode_pemasok, 1) + 1));

      if ($request->ajax()) {
        $pemasok = DB::table(DB::raw('pemasoks, (SELECT @rownum := 0) r'))
          ->select('pemasoks.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->orderBy('kode_pemasok', 'asc')->get();
        return Datatables::of($pemasok)
          ->addColumn('action', function ($row) {
            $btn = '<div class="dropdown d-inline">';
            $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
            $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
            $btn = $btn . '<a class="dropdown-item has-icon editPemasok" href="javascript:void(0)"  data-id="' . $row->id . '" data-kode_pemasok="' . $row->kode_pemasok . '" data-nama_pemasok="' . $row->nama_pemasok . '" data-alamat="' . $row->alamat . '" data-kota="' . $row->kota . '" data-no_telp="' . $row->no_telp . '"><i class="far fa-edit"></i> Edit</a>';
            $btn = $btn . '<a class="dropdown-item has-icon deletePemasok" href="javascript:void(0)" data-id="' . $row->id . '" data-nama_pemasok="' . $row->nama_pemasok . '" ><i class="fas fa-trash"></i> Hapus</a>';
            $btn = $btn . '</div>';
            $btn = $btn . '</div>';
            return $btn;
          })
          ->rawColumns(['action'])
          ->make(true);
      }
      return view('pemasok.index')->with(['title' => "Pemasok", 'kode' => $kode]);
    } else {
      return back();
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_pasok == 1 && Auth::check() == true) {
      if (empty($request->id)) {
        $check_kode_pemasok = Pemasok::where('kode_pemasok', $request->kode_pemasok)->count();
        if ($check_kode_pemasok != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Kode Pemasok sudah tersedia harap refresh terlebih dahulu', 401]);
        } else {
          $pemasok = new Pemasok;
          $pemasok->id = Str::uuid();
          $pemasok->kode_pemasok = $request->kode_pemasok;
          $pemasok->nama_pemasok = $request->nama_pemasok;
          $pemasok->alamat = $request->alamat;
          $pemasok->kota = $request->kota;
          $pemasok->no_telp = $request->no_telp;
          $pemasok->save();
          return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
        }
      } else {
        $pemasok = Pemasok::where('id', $request->id)->update($request->all());
        return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
      }
    } else {
      return back();
    }
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_pasok == 1 && Auth::check() == true) {
      Pemasok::find($id)->delete();
      return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    } else {
      return back();
    }
  }
}
