<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Produk;
use App\Models\Acces;
use Yajra\DataTables\DataTables;

class ProdukController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function index(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {

      if ($request->ajax()) {
        $produk = DB::table(DB::raw('produks, (SELECT @rownum := 0) r'))
          ->select('produks.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->get();
        return Datatables::of($produk)
          ->addColumn('action', function ($row) {
            $btn = '<div class="dropdown d-inline">';
            $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
            $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
            $btn = $btn . '<a class="dropdown-item has-icon editProduk" href="javascript:void(0)"  data-id="' . $row->id . '" data-nama_produk="' . $row->nama_produk . '"><i class="far fa-edit"></i> Edit</a>';
            $btn = $btn . '<a class="dropdown-item has-icon deleteProduk" href="javascript:void(0)" data-id="' . $row->id . '" data-nama_produk="' . $row->nama_produk . '"><i class="fas fa-trash"></i> Hapus</a>';
            $btn = $btn . '</div>';
            $btn = $btn . '</div>';
            return $btn;
          })
          ->addColumn('created_at', function ($row) {
            return date('d F Y', strtotime($row->created_at));
          })
          ->rawColumns(['action'])
          ->make(true);
      }
      return view('produk.index', ['title' => "Produk"]);
    } else {
      return back();
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {
      if (empty($request->id)) {
        $check_nama_produk = Produk::where('nama_produk', $request->nama_produk)->count();
        if ($check_nama_produk != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Nama Produk Sudah Digunakan', 401]);
        } else {
          $produk = new Produk;
          $produk->id = Str::uuid();
          $produk->nama_produk = $request->nama_produk;
          $produk->save();
          return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
        }
      } else {
        $check_nama_produk_id = Produk::find($request->id);
        $check_nama_produk = Produk::where('nama_produk', $request->nama_produk)->count();
        if ($check_nama_produk != 0  && $check_nama_produk_id->nama_produk != $request->nama_produk) {
          return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Nama Produk Sudah Digunakan', 401]);
        } else {
          Produk::where('id', $request->id)->update($request->all());
          return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
        }
      }
    } else {
      return back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $checkAccess = Acces::where('user_id', Auth::id())->first();
    if ($checkAccess->kelola_barang == 1 && Auth::check() == true) {
      Produk::find($id)->delete();
      return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    } else {
      return back();
    }
  }
}
