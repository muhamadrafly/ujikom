<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\User;
use App\Models\Acces;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function index(Request $request)
  {
    if (Auth::user()->is_admin == 1 && Auth::check() == true) {
      if ($request->ajax()) {
        $users = DB::table(DB::raw('users, (SELECT @rownum := 0) r'))
          ->select('users.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->get();
        return DataTables::of($users)
          ->addColumn('action', function ($row) {
            $btn = '<div class="dropdown d-inline">';
            $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
            $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
            $btn = $btn . '<a class="dropdown-item has-icon editAccount" href="javascript:void(0)"  data-id="' . $row->id . '" data-nama="' . $row->nama . '" data-username="' . $row->username . '" data-email="' . $row->email . '" data-is_admin="' . $row->is_admin . '" data-password="' . $row->password . '"><i class="far fa-edit"></i> Edit</a>';
            $btn = $btn . '<a class="dropdown-item has-icon deleteAccount" href="javascript:void(0)" data-id="' . $row->id . '" data-username="' . $row->username . '"><i class="fas fa-trash"></i> Hapus</a>';
            $btn = $btn . '</div>';
            $btn = $btn . '</div>';
            return $btn;
          })
          ->editColumn('is_admin', function ($row) {
            if ($row->is_admin == 1) {
              $html = 'Administrator';
            } else {
              $html = 'Cashier';
            }
            return $html;
          })
          ->rawColumns(['action'])
          ->make(true);
      }
      return view('account.index', ['title' => "Account"]);
    } else {
      return back();
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if (Auth::user()->is_admin == 1 && Auth::check() == true) {
      if (empty($request->id)) {
        $check_email = User::where('email', $request->email)->count();
        $check_username = User::where('username', $request->username)->count();
        if ($check_username != 0 && $check_email != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Email dan username Sudah Digunakan', 401]);
        } elseif ($check_email != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Email Sudah Digunakan', 401]);
        } elseif ($check_username != 0) {
          return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Username Sudah Digunakan', 401]);
        } else {
          $user = new User;
          $user->id = Str::uuid();
          $user->nama = $request->nama;
          $user->email = $request->email;
          $user->username = $request->username;
          $user->password = Hash::make($request->password);
          $user->is_admin = $request->is_admin;
          $user->save();

          $access = new Acces;
          $access->id = Str::uuid();
          $access->user_id = $user->id;
          $access->kelola_barang = 1;
          $access->kelola_pasok = 1;
          $access->kelola_penjualan = 1;
          $access->kelola_laporan = 1;
          $access->save();
          return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
        }
      } else {
        $user_account = User::find($request->id);
        $check_email = User::where('email', $request->email)->count();
        $check_username = User::where('username', $request->username)->count();
        if ($check_email != 0 && $user_account->email != $request->email && $check_username != 0 && $user_account->username != $request->username) {
          return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Email dan Username Sudah Digunakan', 401]);
        } elseif ($check_username != 0 && $user_account->username != $request->username) {
          return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Username Sudah Digunakan', 401]);
        } elseif ($check_email != 0 && $user_account->email != $request->email) {
          return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Email Sudah Digunakan', 401]);
        } else {
          $user = User::where('id', $request->id)->update($request->all());
          return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
        }
      }
    } else {
      return back();
    }
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if (Auth::user()->is_admin == 1 && Auth::check() == true) {
      User::find($id)->delete();
      Acces::where('user_id', $id)->delete();
      return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    } else {
      return back();
    }
  }
}
