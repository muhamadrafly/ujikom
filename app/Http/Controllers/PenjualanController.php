<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\Acces;
use App\Models\Barang;
use App\Models\Penjualan;
use App\Models\DetailPenjualan;
use App\Models\Tampung_bayar;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checkAccess = Acces::where('user_id', Auth::id())->first();
        if ($checkAccess->kelola_penjualan == 1) {
            $lastId = Penjualan::select('no_faktur')->orderBy('no_faktur', 'desc')->first();
            $kode = ($lastId == null ? 'T' . date('dmy') . '0001' : sprintf('T%04d', substr($lastId->no_faktur, 1) + 1));
            return view('penjualan.index', ['title' => "Penjualan", 'kode' => $kode]);
        } else {
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $penjualan = new Penjualan;
        $penjualan->id = Str::uuid();
        $penjualan->no_faktur = $request->no_faktur;
        $penjualan->tgl_faktur = $request->tgl_faktur;
        $penjualan->total_bayar = $request->total_bayar;
        $penjualan->pelanggan_id = $request->pelanggan_id;
        $penjualan->user_id = Auth::id();
        $penjualan->save();

        $tam_bayar = new Tampung_bayar;
        $tam_bayar->id = Str::uuid();
        $tam_bayar->penjualan_id = $penjualan->id;
        $tam_bayar->total = $request->total_bayar;
        $tam_bayar->terima = $request->bayar;
        $tam_bayar->kembali = $request->kembali;
        $tam_bayar->save();

        $jml_barang = count($request->barang_id);
        for ($i = 0; $i < $jml_barang; $i++) {
            $det_penjualan = new DetailPenjualan;
            if (!empty($request->barang_id[$i]) && $request->sub_total != 0) {
                $det_penjualan->id = Str::uuid();
                $det_penjualan->penjualan_id = $penjualan->id;
                $det_penjualan->barang_id = $request->barang_id[$i];
                $det_penjualan->harga_jual = $request->harga_jual[$i];
                $det_penjualan->jumlah = $request->jumlah[$i];
                $det_penjualan->sub_total = $request->sub_total[$i];
                $det_penjualan->save();

                $data_barang = Barang::where('id', $request->barang_id[$i])->first();
                $stok_brang_baru = $data_barang->stok - $request->jumlah[$i];
                Barang::where('id', $request->barang_id[$i])->update(['stok' => $stok_brang_baru]);
            }
        };

        $lastId = Penjualan::select('no_faktur')->orderBy('no_faktur', 'desc')->first();
        $kode = ($lastId == null ? 'T' . date('dmy') . '0001' : sprintf('T%04d', substr($lastId->no_faktur, 1) + 1));
        return response()->json(['kode' => $kode, 'title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
