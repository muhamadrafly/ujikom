<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Barang;
use App\Models\PenarikanBarang;
use Yajra\DataTables\DataTables;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PenarikanBarangExport;
use PDF;

class PenarikanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data_penarikan = DB::table(DB::raw('penarikan_barangs, (SELECT @rownum := 0) r'))
            ->select('penarikan_barangs.*', DB::raw('@rownum := @rownum + 1 AS rownum'))->get();
        if ($request->ajax()) {
            return DataTables::of($data_penarikan)
                ->addColumn('action', function ($row) {
                    $btn = '<div class="dropdown d-inline">';
                    $btn = $btn . '<button class="btn btn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</button>';
                    $btn = $btn . '<div class="dropdown-menu" x-placement="bottom-start" will-change: transform;">';
                    $btn = $btn . '<a class="dropdown-item has-icon editPenarikanBarang" href="javascript:void(0)"  data-id="' . $row->id . '" data-barang_id="' . $row->barang_id . '" data-tanggal_expired="' . $row->tanggal_expired . '" data-ditarik="' . $row->ditarik . '"><i class="far fa-edit"></i> Edit</a>';
                    $btn = $btn . '<a class="dropdown-item has-icon deletePenarikanBarang" href="javascript:void(0)" data-id="' . $row->id . '"><i class="fas fa-trash"></i> Hapus</a>';
                    $btn = $btn . '</div>';
                    $btn = $btn . '</div>';
                    return $btn;
                })
                ->addColumn('kode_barang', function ($row) {
                    $barang = Barang::where('id', $row->barang_id)->first();
                    return $barang->kode_barang;
                })
                ->addColumn('nama_barang', function ($row) {
                    $barang = Barang::where('id', $row->barang_id)->first();
                    return $barang->nama_barang;
                })
                ->addColumn('stok', function ($row) {
                    $barang = Barang::where('id', $row->barang_id)->first();
                    return $barang->stok;
                })
                ->addColumn('created_at', function ($row) {
                    return date('d F Y', strtotime($row->created_at));
                })
                ->editColumn('ditarik', function ($row) {
                    $today = strtotime(date('Y-m-d'));
                    $expired = strtotime($row->tanggal_expired);
                    if ($expired <= $today) {
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" disabled="" checked class="custom-switch-input">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    } else {
                        $toggle = '<label class="custom-switch">';
                        $toggle = $toggle . '<input type="checkbox" disabled="" class="custom-switch-input">';
                        $toggle = $toggle . '<span class="custom-switch-indicator"></span>';
                        $toggle = $toggle . '</label>';
                    }
                    return $toggle;
                })
                ->rawColumns(['action', 'ditarik'])
                ->make(true);
        }
        return view('penarikan_barang.index', ['title' => "Penarikan Barang Kadaluarsa"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Excel::download(new PenarikanBarangExport, 'penarikan_barang-' . time() . '-' . rand() . '.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $check_kode_barang = PenarikanBarang::where('barang_id', $request->barang_id)->count();
            if ($check_kode_barang != 0) {
                return response()->json(['icon' => 'error', 'title' => 'Create Fail', 'message' => 'Barang Sudah ada', 401]);
            } else {
                $penarikan_barang = new PenarikanBarang;
                $penarikan_barang->id = Str::uuid();
                $penarikan_barang->barang_id = $request->barang_id;
                $penarikan_barang->tanggal_expired = $request->tanggal_expired;
                $penarikan_barang->save();
                return response()->json(['title' => 'Create Success', 'message' => 'Data Berhasil Disimpan', 200]);
            }
        } else {
            $check_kode_barang = PenarikanBarang::where('barang_id', $request->barang_id)->count();
            if ($check_kode_barang != 0) {
                return response()->json(['icon' => 'error', 'title' => 'Edit Fail', 'message' => 'Barang Sudah ada', 401]);
            } else {
                PenarikanBarang::where('id', $request->id)->update($request->all());
                return response()->json(['title' => 'Edit Success', 'message' => 'Data Berhasil Diubah', 200]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_barang = Barang::where('id', $id)->first();
        $produk = $data_barang->Produks->nama_produk;
        return response()->json(['barang' => $data_barang, 'produk' => $produk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PenarikanBarang::get();
        $pdf = PDF::loadView('penarikan_barang.export_pdf', compact('data'));
        return $pdf->stream('penarikan_barang-' . time() . '-' . rand() . '.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // if ($request->ditarik == 1) {
        //     PenarikanBarang::where('barang_id', $request->barang_id)->update(['ditarik' => 0]);
        // } else {
        //     PenarikanBarang::where('barang_id', $request->barang_id)->update(['ditarik' => 1]);
        // }
        // return response()->json(['title' => 'Change Success', 'message' => 'Status Berhasil Diubah', 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PenarikanBarang::find($id)->delete();
        return response()->json(['title' => 'Delete Success', 'message' => 'Data Berhasil Dihapus', 200]);
    }
}
